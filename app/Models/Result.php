<?php

namespace MyApp\Models {

  use EasilyPHP\Database\SqlMySQL;

    class Result
  {
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    
    }
    /**
     * Obtiene un registro de resultados
     */

    public function getAllResult()
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM results");
      $this->db->disconnect();
      return $this->db->getAll($result);
    }

    public function getResult($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM results WHERE id=" . $id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }

    
    public function insertResult($result)
    {
      // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
      // https://www.php.net/manual/es/mysqli-stmt.bind-param.php

      $this->db->connect();
      $sql = "INSERT INTO  results(questionnaire_id,min_value,max_value,feedback) VALUES (?,?,?,?)";

      if ($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("iiis", $result['questionnaire_id'], $result['min_value'], $result['max_value'],$result['feedback']);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    
    }
     /**
     * Actualiza un registro de "user" en la base de datos
     * @param questionnaire contiene los datos del registro de usuario a actualizar
     */
    public function updateResult($result)
    {
      $this->db->connect();

        $sql = "UPDATE results SET questionnaire_id = ?, min_value = ?, max_value = ?, feedback = ? WHERE id = ?";

        $stmt = $this->db->prepareSQL($sql);
      if ($stmt) {
          $stmt->bind_param("iiisi", $result['questionnaire_id'], $result['min_value'], $result['max_value'], $result['feedback'], $result['id']);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }


    public function enviarquestionnaire(){

      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM questionnaires q INNER JOIN results qu ON q.id=qu.questionnaire_id"); 
      $this->db->disconnect();
      return $this->db->getAll($result);


    }


    /**
     * Elimina un registro de la base de datos
     * @param id del registro que se desea eliminar
     */
    public function deleteResult($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM results WHERE id = ?";

      if ($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }

    /**
     * Authentica un usuario
     */
  }

    
  
  
  }