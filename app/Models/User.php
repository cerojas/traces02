<?php namespace MyApp\Models {

use EasilyPHP\Database\SqlMySQL;


class User
{
  private $db = null;

  public function __construct($config)
  {
    $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
  }

  /**
   * Obtiene todos los registros de usuario
   */
  public function getAllUsers()
  {
    $this->db->connect();
    $result = $this->db->runSql("SELECT * FROM users");
    $this->db->disconnect();
    return $this->db->getAll($result);
  }

  

  /**
   * Obtiene un registro de usuario
   */
  public function getUser($id)
  {
    $this->db->connect();
    $result = $this->db->runSql("SELECT * FROM users WHERE id=" . $id);
    $this->db->disconnect();
    return $this->db->nextResultRow($result);
  }

  /**
   * Inserta un nuevo registro de usuario en base de datos
   * @param user contiene los datos del nuevo registro de usuario
   */
  public function insertUser($user)
      {
        // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
        // https://www.php.net/manual/es/mysqli-stmt.bind-param.php

        $this->db->connect();
        $sql = "INSERT INTO users (`fullname`, `username`, `password`) VALUES (?, ?, ?)";
        
        if($stmt = $this->db->prepareSQL($sql)) {
          $stmt->bind_param("sss", $user['fullname'], $user['username'], $user['password']);
   
          $stmt->execute();
         
          $stmt->close();
        
        } else {
            echo $this->db->getError();
            exit;
        }
        $this->db->disconnect();
      }

  /**
   * Actualiza un registro de "user" en la base de datos
   * @param user contiene los datos del registro de usuario a actualizar
   */
  public function updateUser($user)
      {
        $this->db->connect();
        if ($user['password'] == "")
          $sql = "UPDATE users SET fullname = ?, username = ?, role = ?, blocked = ? WHERE id = ?";
        else
          $sql = "UPDATE users SET fullname = ?, username = ?, role = ?, blocked = ?, password = ? WHERE id = ?";

        $stmt = $this->db->prepareSQL($sql);
        if($stmt) {
          if ($user['password'] == "")
            $stmt->bind_param("ssssi", $user['fullname'], $user['username'], $user['role'], $user['blocked'], $user['id']);
          else
            $stmt->bind_param("sssssi", $user['fullname'], $user['username'], $user['role'], $user['blocked'], $user['password'], $user['id']);
          $stmt->execute();
          $stmt->close();
        } else {
            echo $this->db->getError();
            exit;
        }
        $this->db->disconnect();
      }


  /**
   * Elimina un registro de la base de datos
   * @param id del registro que se desea eliminar
   */
  public function deleteUser($id)
  {
    $this->db->connect();
    $sql = "DELETE FROM users WHERE id = ?";

    if ($stmt = $this->db->prepareSQL($sql)) {
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $stmt->close();
    } else {
      echo $this->db->getError();
      exit;
    }
    $this->db->disconnect();
  }

  /**
   * Authentica un usuario
   */
  public function authenticate($username, $password)
  {
    $sql = "SELECT id, username, fullname, role, blocked FROM users " .

      " WHERE username = '" . $username . "'" .

      "   AND password = '" . $password . "'";

    $this->db->connect();
    $result = $this->db->runSql($sql);
    $this->db->disconnect();
    return $this->db->nextResultRow($result);
  }

  public function userExists($username)
  {
      $this->db->connect();

      /* Prepared statement, stage 1: prepare */
      if (!($stmt = 
          $this->db->prepareSql("SELECT count(1) as `exists` FROM users WHERE `username` = ?"))) {
          echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
      }

      /* Prepared statement, stage 2: bind and execute */
      if (!$stmt->bind_param("s", $username)) {
          echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
      }

      if (!$stmt->execute()) {
          echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
      }

      $result = $stmt->get_result();
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
  }
}
}
