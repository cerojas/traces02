<?php

namespace MyApp\Models {

  use EasilyPHP\Database\SqlMySQL;

  class Question
  {
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    }

    /**
     * Obtiene todos los registros de usuario
     */
    public function getAllQuestion()
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM questions");
      $this->db->disconnect();
      return $this->db->getAll($result);
    }

    public function enviarquestionnaire(){

      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM questionnaires q INNER JOIN questions qu ON q.id=qu.questionnaire_id"); 
      $this->db->disconnect();
      return $this->db->getAll($result);


    }

    /**
     * Obtiene un registro de usuario
     */
    public function getQuestions($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM questions WHERE id=" . $id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }

    /**
     * Inserta un nuevo registro de cuestonario en base de datos
     * @param user contiene los datos del nuevo registro de usuario
     */
    public function insertQuestion($question)
    {
      // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
      // https://www.php.net/manual/es/mysqli-stmt.bind-param.php

      $this->db->connect();
      $sql = "INSERT INTO questions(questionnaire_id,question_text) VALUES (?, ?)";

      if ($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("is", $question['questionnaire_id'], $question['question_text']);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }

    /**
     * Actualiza un registro de "user" en la base de datos
     * @param user contiene los datos del registro de usuario a actualizar
     */
    public function updateQuestion($question)
    {
      $this->db->connect();
        $sql = "UPDATE questions SET question_text = ? WHERE id = ?";
      $stmt = $this->db->prepareSQL($sql);
      if ($stmt) {
          $stmt->bind_param("si",$question['question_text'],$question['id']);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }


    /**
     * Elimina un registro de la base de datos
     * @param id del registro que se desea eliminar
     */
    public function deleteQuestion ($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM questions WHERE id = ?";

      if ($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }

    
    /**
     * Authentica un usuario
     */
  }
}
