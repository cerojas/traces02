<?php

namespace MyApp\Models {

  use EasilyPHP\Database\SqlMySQL;

  class Questionnaire
  {
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    }

    /**
     * Obtiene todos los registros de usuario
     */

    public function buscarpreguntas($id){
     
      //$this->db->connect();
      //selecciona las preguntas
      //$result = $this->db->runSql("SELECT * FROM questions where questionnaire_id=" . $id);
      //$this->db->disconnect();
      //return $this->db->getAll($result);


      $this->db->connect();
  
      $result = $this->db->runSql("SELECT questionnaire_id,question_text FROM questions
      INNER JOIN questionnaires 
      ON (questionnaires.id=questions.questionnaire_id) 
      WHERE questionnaires.id=" . $id);
      $this->db->disconnect();

      return $this->db->getAll($result);
  
      
    }

    public function buscarrespuestas($id){

      $this->db->connect();
      
      $result = $this->db->runSql( "SELECT a.answer_text ,a.question_id, q.questionnaire_id, q.question_text 
      FROM questions q INNER JOIN questionnaires qu ON qu.id=q.questionnaire_id 
      INNER JOIN answers a ON a.question_id = q.id   WHERE qu.id=" . $id);

     // $result = $this->db->runSql("SELECT a.answer_text ,a.question_id FROM questions q  INNER JOIN answers a ON a.question_id = q.id"
    
//     );
       
        $this->db->disconnect();
        return $this->db->getAll($result);


        
    }

    public function nombrecuestionnaire(){

      $this->db->connect();
      $result = $this->db->runSql( "SELECT qu.description 
      FROM questionnaires qu INNER JOIN questionnaires qu ON qu.id=q.questionnaire_id 
      INNER JOIN answers a ON a.question_id = q.id   WHERE qu.id=" . $id);
          $this->db->disconnect();
          return $this->db->getAll($result);


    }


    


    public function getAllQuestionnaires()
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM questionnaires");
      $this->db->disconnect();
      return $this->db->getAll($result);
    }

    /**
     * Obtiene un registro de usuario
     */
    public function getQuestionnaire($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM questionnaires WHERE id=" . $id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }

    /**
     * Select para presentar el resultado segun el id cuestionario
     */
    public function Tried($id){
      
      $this->db->connect();
      $result = $this->db->runSql("SELECT *
      FROM results re INNER JOIN users_questionnaires uq ON re.questionnaire_id=uq.questionnaire_id 
      WHERE uq.questionnaire_id=" . $id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);

    }

    public function Points(){
		
      $this->db->connect();
        $result = $this->db->runSql("SELECT *
        FROM users u INNER JOIN users_questionnaires uq ON u.id=uq.user_id "); 
        $this->db->disconnect();
        return $this->db->getAll($result);
    }
    
    public function CantQues(){
      
      $this->db->connect();
        $result = $this->db->runSql("SELECT  u.fullname, count(c.user_id)/(SELECT COUNT(*) FROM questionnaires) as canquest
  FROM users_questionnaires c 
  INNER JOIN users u on u.id=c.user_id GROUP BY c.user_id
  order by c.user_id Desc LIMIT 5"); 
        $this->db->disconnect();
        return $this->db->getAll($result);
    }

    

    /**
     * Inserta un nuevo registro de cuestonario en base de datos
     * @param questionnaire contiene los datos del nuevo registro de usuario
     */
    public function insertQuestionare($questionnaire)
    {
      // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
      // https://www.php.net/manual/es/mysqli-stmt.bind-param.php

      $this->db->connect();
      $sql = "INSERT INTO questionnaires(description,long_description) VALUES (?, ?)";

      if ($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("ss", $questionnaire['description'], $questionnaire['long_description']);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }

    /**
     * Actualiza un registro de "user" en la base de datos
     * @param questionnaire contiene los datos del registro de usuario a actualizar
     */
    public function updateQuestionnaire($questionnaire)
    {
      $this->db->connect();

        $sql = "UPDATE questionnaires SET description = ?, long_description = ?  WHERE id = ?";

        $stmt = $this->db->prepareSQL($sql);
      if ($stmt) {
          $stmt->bind_param("ssi", $questionnaire['description'], $questionnaire['long_description'], $questionnaire['id']);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }

    public function alertdelete(){
      $this->db->connect();
      //seleccionar si question.questionnaire_id = questionnaire.id
      $result = ("SELECT *
      FROM questions q INNER JOIN questionnaires qu ON qu.id=q.questionnaire_id");
      $this->db->disconnect();
      return $this->db->getAll($result);

    }

   public function validar($id1){

    $this->db->connect();
    //seleccionar si question.questionnaire_id = questionnaire.id
    $result = ("SELECT *
    FROM questionnaires qu INNER JOIN questions q ON qu.id= q.questionnaire_id");
    $this->db->disconnect();
    return $this->db->getAll($result);

   } 


    /**
     * Elimina un registro de la base de datos
     * @param id del registro que se desea eliminar
     */
    public function deleteQuestionaire($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM questionnaires WHERE id = ?";

      if ($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }

    /**
     * Authentica un usuario
     */
  

  public function deleteTried($id)
{
  $this->db->connect();
  $sql="SELECT * FROM questionnaires INNER JOIN questions ON questionnaires.id=question.questionnaire_id";
  
  $sql = "DELETE FROM users_questionnaires WHERE id = ?";

  if ($stmt = $this->db->prepareSQL($sql)) {
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->close();
  } else {
    echo $this->db->getError();
    exit;
  }
  $this->db->disconnect();
}

/**
 * Authentica un usuario
 */
}
}






