<?php

namespace MyApp\Models {

  use EasilyPHP\Database\SqlMySQL;

  class Answer
  {
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    }

    /**
     * Obtiene todos los registros de usuario
     */
    public function getAllAnswers()
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM answers");
      $this->db->disconnect();
      return $this->db->getAll($result);
    }

    /**
     * Obtiene un registro de usuario
     */
    public function getAnswers($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM answers WHERE id=" . $id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }


    public function enviarid(){
      
      $this->db->connect();
  
      $result = $this->db->runSql("SELECT question_text FROM questions
      INNER JOIN answers 
      ON (questions.id=answers.question_id) 
      WHERE   answers.question_id = questions.id");
      $this->db->disconnect();

      return $this->db->getAll($result);


    }

    public function enviarid2(){
      
      $this->db->connect();
  
      $result = $this->db->runSql("SELECT question_text FROM questions
      INNER JOIN answers 
      ON (questions.id=answers.question_id) 
      WHERE   questions.id = answers.question_id ");
      $this->db->disconnect();

      return $this->db->nextResultRow($result);


    }
  

 
  

    /**
     * Inserta un nuevo registro de cuestonario en base de datos
     * @param user contiene los datos del nuevo registro de usuario
     */
    public function insertAnswer($answer)
    {
      // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
      // https://www.php.net/manual/es/mysqli-stmt.bind-param.php

      $this->db->connect();
      $sql = "INSERT INTO answers(question_id,number,answer_text,answer_points) VALUES (?, ?, ?, ?)";

      if ($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("iisi", $answer['question_id'], $answer['number'],$answer['answer_text'],$answer['answer_points']);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }

    /**
     * Actualiza un registro de "user" en la base de datos
     * @param user contiene los datos del registro de usuario a actualizar
     */
    public function updateAnswer($answer)
    {
      $this->db->connect();
        $sql = "UPDATE answers SET answer_text = ?, answer_points = ? WHERE id = ?";
      $stmt = $this->db->prepareSQL($sql);
      if ($stmt) {
          $stmt->bind_param("sii",$answer['answer_text'],$answer['answer_points'],$answer['id']);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }


    /**
     * Elimina un registro de la base de datos
     * @param id del registro que se desea eliminar
     */
    public function deleteAnswer ($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM answers WHERE id = ?";

      if ($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
        echo $this->db->getError();
        exit;
      }
      $this->db->disconnect();
    }

    
    /**
     * Authentica un usuario
     */
  }
}
