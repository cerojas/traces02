<?php namespace MyApp\Controllers {

use MyApp\Models\{Questionnaire,Question,Answer,User};
use MyApp\Utils\Message;

class QuestionnairesController
{
    private $config = null;
    private $login = null;
    private $message = null;

    /**
     * Constructor de controlador de Questonaire
     * 
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del usuario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
        $this->message = new Message();
    }

    /**
     * Muestra la vista "Index" del catálogo de usuarios
     */
    public function index(){
       
        $login =$this->login  ;
        $message = $this->message;
        $questionnaireModel = new Questionnaire($this->config);
        $collection = $questionnaireModel->getAllQuestionnaires();

        $dataToView = ["collection" => $collection,
                       "login" => $this->login ];
                       //var_dump($this->login);
                       //exit;

        return view("questionnaire/index.php", compact("collection","message","login"));
    }

    public function list(){
        $questionnaireModel = new Questionnaire($this->config);
        $collection = $questionnaireModel->getAllQuestionnaires();

        $dataToView = ["collection" => $collection,
                       "login" => $this->login ];
                       //var_dump($this->login);
                       //exit;

        return view("questionnaire/listquestionnaire.php", $dataToView);
    }

    public function result(){

        return view("questionnaire/results.php");
    
    }

    public function reportquestionnaire(){
        $questionnaireModel = new Questionnaire($this->config);
        $collection = $questionnaireModel->getAllQuestionnaires();
        $dataToView = ["login" => $this->login,
        "collection" => $collection];

        return view("questionnaire/reportquestionnaire.php",$dataToView);

    }

    public function reportemployee(){

        $userModel = new User($this->config);
        $collection = $userModel->getAllUsers();

        $questionnaireModel = new Questionnaire($this->config);
        $questionnaire = $questionnaireModel->getAllQuestionnaires();
        $dataToView = ["login" => $this->login,
        "collection" => $collection,
                   "questionnaire" => $questionnaire];

        return view("questionnaire/report.php",$dataToView);
    }

    public function myresult(){

        $dataToView = [
        "login" => $this->login ];
        //var_dump($this->login);
        //exit;

        return view("questionnaire/myresults.php",$dataToView);
    }

    public function questionnaire($id){

        $questionModel = new question($this->config);
        $collection = $questionModel->getAllQuestion();

        $answerModel = new Answer($this->config);
        $collection2 = $answerModel->getAllAnswers();

        $questionaireModel = new questionnaire($this->config);
     
        $question = $questionaireModel->buscarpreguntas($id);
        $answer = $questionaireModel->buscarrespuestas($id);
        $questionnaire = $questionaireModel->getQuestionnaire($id);


        

         $dataToView = [
                         "answer" => $answer,
                         "question" => $question,
                         "collection" => $collection,
                         "questionnaire" => $questionnaire,
                       "login" => $this->login ];

                   


        return view("questionnaire/questionnairesanwers.php", $dataToView);
    }

    /**
     * Muestra el formulario para crear un nuevo registro
     */
    public function create()
    {
        $dataToView = ["login" => $this->login ];
        return view("questionnaire/new.php", $dataToView);
    }

    /**
     * Almacena un registro nuevo de "user" en la base de datos
     * @param request contiene los datos del nuevo registro de usuario
     */
    public function store($request)
    {   
        $login =$this->login  ;
        $message = new Message();
        $description         = $_POST["description"];
        $long_description         = $_POST["long_description"];
        if ((ltrim($description) == "") || (ltrim($long_description) == "")){
        
            $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
            view("questionnaire/new.php", compact("message","login"));
           
         } else {

        $questionnaireModel = new Questionnaire($this->config);
        $questionnaireModel->insertQuestionare($request);
        
         $message->setSuccessMessage(null, "El cuestionario se agregó correctamente, presione Regresar para ir a la lista de cuestionarios", null, true);
         view("questionnaire/new.php", compact("message","login"));
       // header('Location: /questionnaire');
      
        
       // view("question/new.php", compact("login","description"));

        }

       
    }

     /**
         * Muestra para vista "show" con los detalles del registro "id"
         * @param id del registro del cual se deben mostrar los detalles
         */
        public function show($id){
            $questionnaireModel = new Questionnaire($this->config);
            $questionnaire = $questionnaireModel->getQuestionnaire($id);
            $dataToView = ["login" => $this->login,
                           "questionnaire" => $questionnaire ];
            return view("questionnaire/show.php", $dataToView);
        }

        /**
        * Muestra para vista "edit" con los detalles del registro "id"
        * @param id del registro del cual se deben cargar los detalles para edición
        */
        public function edit($id){
            $questionnaireModel = new Questionnaire($this->config);
            $questionnaire = $questionnaireModel->getQuestionnaire($id);
            $dataToView = ["login" => $this->login,
                           "questionnaire" => $questionnaire ];
            return view("questionnaire/edit.php", $dataToView);
        }

        /**
        * Actualiza un registro de "user" en la base de datos
        * @param request contiene los datos del registro de usuario a actualizar
        */
        public function update($request)
        {
           
            $questionnaireModel = new Questionnaire($this->config);
            $questionnaireModel->updateQuestionnaire($request);
            $this->message->setSuccessMessage(null, "Se actualizo el registro correctamente", null, true);
            $this->index();
           
          //  header('Location: /questionnaire');
        }

        public function graphic(){

            $userModel = new Questionnaire($this->config);
            $collection = $userModel->CantQues();
            
    
            $questionnaireModel = new Questionnaire($this->config);
            $questionnaire = $questionnaireModel->getAllQuestionnaires();
            $dataToView = ["login" => $this->login,
            "collection" => $collection,
                       "questionnaire" => $questionnaire];
    
            return view("questionnaire/graphic.php",$dataToView);
        }

        /**
         * Elimina un registro de la base de datos
         * @param id del registro que se desea eliminar
         */
        public function destroy($id)
        {
            //$id1         = $_POST["id"];
            //$questionnaireModel1 = new Questionnaire($this->config);
            //$questionnaireModel1->validar($id1);
            //var_dump($questionnaireModel1);
            //exit;
            

            //if($questionnaffected_rowsaireModel1==1){

              
                //$this->message->setSuccessMessage(null, "No se puede eliminar el registro correctamente", null, true);
                //$this->index();

           // }else{
          // if($questionnaireModel->num_rows==1){
              //  $this->message->setSuccessMessage(null, "Se  el registro correctamente", null, true);
              // $this->index();

          // }else{
            //if ($questionnaireModel==0)  {
            $questionnaireModel = new Questionnaire($this->config);
            $questionnaireModel->deleteQuestionaire($id);
            $this->message->setSuccessMessage(null, "Se eliminó el registro correctamente", null, true);
          //  var_dump($questionnaireModel);
            $this->index();
        //}else{
         
         //   $this->message->setSuccessMessage(null, "Se  el registro correctamente", null, true);
            //  var_dump($questionnaireModel);
            //  $this->index();

        //}
    }
    

        public function destroyTried($id)
        {
            $questionnaireModel = new Questionnaire($this->config);
            $questionnaireModel->deleteTried($id);
            header('Location: /questionnaire/reportquestionnaire');
        }
        
        public function showTried($id){
            $questionnaireModel = new Questionnaire($this->config);
            $questionnaire = $questionnaireModel->Tried($id);
            $dataToView = ["login" => $this->login,
                           "questionnaire" => $questionnaire ];
            return view("questionnaire/showresult.php", $dataToView);
        }

        
}
}



