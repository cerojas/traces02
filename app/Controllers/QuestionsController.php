<?php namespace MyApp\Controllers {

use MyApp\Models\{Question,Questionnaire};
use MyApp\Utils\Message;


class QuestionsController
{
    private $config = null;
    private $login = null;
    private $message = null;

    /**
     * Constructor de controlador de Questonaire
     * 
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del usuario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
        $this->message = new Message();
    }

    /**
     * Muestra la vista "Index" del catálogo de usuarios
     */
    public function index(){
        $login =$this->login  ;
        $message = $this->message;
        $questionModel = new question($this->config);
        $collection = $questionModel->enviarquestionnaire();
        $questionnairesModel = new Questionnaire($this->config);
        $questionnaires = $questionnairesModel->getAllQuestionnaires();
         $dataToView = ["collection" => $collection,
                       "login" => $this->login,
                    "questionnaires" => $questionnaires ];
                       //var_dump($this->login);
                       //exit;

        return view("question/index.php", compact("collection","message","login","questionnaires"));
    }

    /**
     * Muestra el formulario para crear un nuevo registro
     */
    public function create()
    {   

      
        $questionnaireModel = new Questionnaire($this->config);
        $questionnaire = $questionnaireModel->getAllQuestionnaires();
        $dataToView = ["login" => $this->login,
                   "questionnaire" => $questionnaire];
        return view("question/new.php", $dataToView);
        
    }

    /**
     * Almacena un registro nuevo de "user" en la base de datos
     * @param request contiene los datos del nuevo registro de usuario
     */
    public function store($request)
    {   
        $questionnaireModel = new Questionnaire($this->config);
        $questionnaire = $questionnaireModel->getAllQuestionnaires();

        $login =$this->login  ;
        $message = new Message();

        $question_text         = $_POST["question_text"];
        $id         = $_POST["questionnaire_id"];
       
        if ((ltrim($question_text) == "") || (ltrim($id) == "")){
        
            $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
            view("question/new.php", compact("message","login","questionnaire"));
           
         } else {
        $questionModel = new Question($this->config);
        $questionModel->insertQuestion($request);
        $message->setSuccessMessage(null, "El cuestionario se agregó correctamente, presione Regresar para ir a la lista de cuestionarios", null, true);
        view("question/new.php", compact("message","login","questionnaire"));
    }
}
     /**
         * Muestra para vista "show" con los detalles del registro "id"
         * @param id del registro del cual se deben mostrar los detalles
         */
        public function show($id){
            $questionModel = new question($this->config);
            $question = $questionModel->getQuestions($id);
            $dataToView = ["login" => $this->login,
                          "question" => $question ];
            return view("question/show.php", $dataToView);
        }

        /**
        * Muestra para vista "edit" con los detalles del registro "id"
        * @param id del registro del cual se deben cargar los detalles para edición
        */
        public function edit($id){
            $questionModel  = new question($this->config);
            $question= $questionModel->getQuestions($id);
            $dataToView = ["login" => $this->login,
                           "question" => $question];
            return view("question/edit.php", $dataToView);
        }

        /**
        * Actualiza un registro de "user" en la base de datos
        * @param request contiene los datos del registro de usuario a actualizar
        */
        public function update($request)
        {
            $questionModel = new question($this->config);
            $questionModel->updateQuestion($request);
            $this->message->setSuccessMessage(null, "Se actualizo el registro correctamente", null, true);
            $this->index();
        }

        /**
         * Elimina un registro de la base de datos
         * @param id del registro que se desea eliminar
         */
        public function destroy($id)
        {
            $questionModel = new question($this->config);
            $questionModel->deleteQuestion($id);
            
            $this->message->setSuccessMessage(null, "Se eliminó el registro correctamente", null, true);
            $this->index();
        }

    
}
}