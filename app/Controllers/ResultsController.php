<?php namespace MyApp\Controllers {

use MyApp\Models\{Result,Questionnaire};
use MyApp\Utils\Message;

class ResultsController
{
    private $config = null;
    private $login = null;
    private $message = null;
    

    /**
     * Constructor de controlador de Questonaire
     * 
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del usuario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
        $this->message = new Message();
    }

    /**
     * Muestra la vista "Index" del catálogo de resultados
     */
    public function index(){
        $login =$this->login  ;
        $message = $this->message;
        $resultModel = new result($this->config);
        $collection = $resultModel->enviarquestionnaire();

        $dataToView = ["collection" => $collection,
                       "login" => $this->login ];
                       //var_dump($this->login);
                       //exit;

        return view("result/index.php", compact("collection","message","login"));
    }

    /**
     *Muestra formulario de resultados
     */
    public function create()
    {   
       
        $questionnaireModel = new Questionnaire($this->config);
        $questionnaire = $questionnaireModel->getAllQuestionnaires();
        $dataToView = ["login" => $this->login,
                   "questionnaire" => $questionnaire];
        return view("result/new.php", $dataToView);
        
    } 


    /**
     * Almacena un registro nuevo de "Resultados" en la base de datos
     * @param request contiene los datos del nuevo registro de usuario
     */
    
    public function store($request)
    {
        $questionnaireModel = new Questionnaire($this->config);
        $questionnaire = $questionnaireModel->getAllQuestionnaires();
        $login =$this->login  ;
        $message = new Message();

        $questionnaire_id         = $_POST["questionnaire_id"];
        $min_value         = $_POST["min_value"];
        $max_value         = $_POST["max_value"];
        $feedback         = $_POST["feedback"];
       
        if ((ltrim($questionnaire_id) == "") || (ltrim($min_value) == "") || (ltrim($max_value) == "") || (ltrim($feedback) == "")){
        
            $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
            view("result/new.php", compact("message","login","questionnaire"));
           
         } else {
        $resultModel = new Result($this->config);
        $resultModel->insertResult($request);
        $message->setSuccessMessage(null, "El resultado se agregó correctamente, presione Regresar para ir a la lista de resultados", null, true);
        view("result/new.php", compact("message","login","questionnaire"));
    }
        //header('Location: /result');
    }

    /**
         * Muestra para vista "show" con los detalles del registro "id"
         * @param id del registro del cual se deben mostrar los detalles
         */
        public function show($id){
            $resultModel = new result($this->config);
            $result = $resultModel->getResult($id);
            $dataToView = ["login" => $this->login,
                          "result" => $result];
            return view("result /show.php", $dataToView);
        }

        /**
        * Muestra para vista "edit" con los detalles del registro "id"
        * @param id del registro del cual se deben cargar los detalles para edición
        */
        public function edit($id){
            $resultModel  = new result($this->config);
            $result= $resultModel->getResult($id);
            $dataToView = ["login" => $this->login,
                           "result" => $result];
            return view("result/edit.php", $dataToView);
        }

        /**
        * Actualiza un registro de "user" en la base de datos
        * @param request contiene los datos del registro de usuario a actualizar
        */
        public function update($request)
        {
            $resultModel = new result($this->config);
            $resultModel->updateResult($request);
            $this->message->setSuccessMessage(null, "Se actualizo el registro correctamente", null, true);
            $this->index();
           // header('Location: /result');
        }

        /**
         * Elimina un registro de la base de datos
         * @param id del registro que se desea eliminar
         */
        public function destroy($id)
        {
            $resultModel = new Result($this->config);
            $resultModel->deleteResult($id);
            $this->message->setSuccessMessage(null, "Se eliminó el registro correctamente", null, true);
            $this->index();
           // header('Location: /result');
        }
}
}