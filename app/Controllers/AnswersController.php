<?php namespace MyApp\Controllers {

use MyApp\Models\{Answer,Question,Questionnaire};
use MyApp\Utils\Message;


class AnswersController
{
    private $config = null;
    private $login = null;
    private $message = null;

    /**
     * Constructor de controlador de Questonaire
     * 
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del usuario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
        $this->message = new Message();
    }

    /**
     * Muestra la vista "Index" del catálogo de usuarios
     */
    public function index(){
        $login =$this->login  ;
        $message = $this->message;
       
        $answerModel = new Answer($this->config);
        $collection = $answerModel->getAllAnswers();
        $question = $answerModel->enviarid();
        $dataToView = ["collection" => $collection,
                       "login" => $this->login,
                       "question" => $question ];
                       // var_dump($question);
                       //exit;
                     

        return view("answer/index.php", compact("collection","login","question","message"));
    }

    /**
     * Muestra el formulario para crear un nuevo registro
     */
    public function create()
    {   

      
        $questionModel = new Question($this->config);
        $question = $questionModel->getAllQuestion();
        $dataToView = ["login" => $this->login,
                   "question" => $question];
        return view("answer/new.php", $dataToView);
        
    }

    /**
     * Almacena un registro nuevo de "user" en la base de datos
     * @param request contiene los datos del nuevo registro de usuario
     */
    public function store($request)
    { 
        $questionModel = new Question($this->config);
        $question = $questionModel->getAllQuestion();

     
        $login =$this->login  ;
        $message = new Message();

        $question_id         = $_POST["question_id"];
        
        $answer_text         = $_POST["answer_text"];
        $number         = $_POST["number"];
        $answer_points         = $_POST["answer_points"];

        if ((ltrim($question_id) == "") || (ltrim($answer_text) == "") || (ltrim($number) == "") || (ltrim($answer_points) == "") ){
        
            $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
            view("answer/new.php", compact("message","login","question"));
           
         } else {
            $answerModel = new Answer($this->config);
            $answerModel->insertAnswer($request);
            $message->setSuccessMessage(null, "El cuestionario se agregó correctamente, presione Regresar para ir a la lista de cuestionarios", null, true);
            view("answer/new.php", compact("message","login","question"));
       // header('Location: /answer');
    }
}
     /**
         * Muestra para vista "show" con los detalles del registro "id"
         * @param id del registro del cual se deben mostrar los detalles
         */
        public function show($id){
            $questionModel = new question($this->config);
            $preguntas = $questionModel->getAllQuestion();
            $answerModel = new Answer($this->config);
            $answer = $answerModel->getAnswers($id);
            $question = $answerModel->enviarid();
            $collection = $answerModel->getAllAnswers();
            $dataToView = ["login" => $this->login,
                          "answer" => $answer, 
                          "question" => $question,
                        "collection" => $collection,
                      "preguntas" => $preguntas ];
                      // var_dump($preguntas);
            return view("answer/show.php", $dataToView);
            
        }

        /**
        * Muestra para vista "edit" con los detalles del registro "id"
        * @param id del registro del cual se deben cargar los detalles para edición
        */
        public function edit($id){
            $answerModel  = new Answer($this->config);
            $answer= $answerModel->getAnswers($id);
            $dataToView = ["login" => $this->login,
                           "answer" => $answer];
            return view("answer/edit.php", $dataToView);
        }

        /**
        * Actualiza un registro de "user" en la base de datos
        * @param request contiene los datos del registro de usuario a actualizar
        */
        public function update($request)
        {
            $answerModel = new Answer($this->config);
            $answerModel->updateAnswer($request);
            $this->message->setSuccessMessage(null, "Se actualizo el registro correctamente", null, true);
            $this->index();
           // header('Location: /answer');
        }

        /**
         * Elimina un registro de la base de datos
         * @param id del registro que se desea eliminar
         */
        public function destroy($id)
        {
            $answerModel = new Answer($this->config);
            $answerModel->deleteAnswer($id);
            $this->message->setSuccessMessage(null, "Se eliminó el registro correctamente", null, true);
            $this->index();
            //header('Location: /answer');
        }

    
}
}