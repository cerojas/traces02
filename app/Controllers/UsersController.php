<?php namespace MyApp\Controllers {

use MyApp\Models\User;
use MyApp\Utils\Message;




class UsersController
{
    private $config = null;
    private $login = null;
    private $message = null;

    /**
     * Constructor de controlador de usuarios
     * 
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del usuario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
        $this->message = new Message();
    }

    /**
     * Muestra la vista "Index" del catálogo de usuarios
     */
    public function index(){
        $login =$this->login  ;
        $message = $this->message;
        $userModel = new User($this->config);
        $collection = $userModel->getAllUsers();

        $dataToView = ["collection" => $collection,
                       "login" => $this->login ];

         view("users/index.php", compact("collection","message","login"));
    }

    public function index1(){
    $userModel = new User($this->config);
       $collection = $userModel->getAllUsers();

    $dataToView = ["collection" => $collection,
                      "login" => $this->login ];

       return view("/dashboard.php", $dataToView);
  }

    /**
     * Muestra el formulario para crear un nuevo registro
     */
    public function create()
    {
        $dataToView = ["login" => $this->login ];
        return view("users/new.php", $dataToView);
    }

    /**
     * Almacena un registro nuevo de "user" en la base de datos
     * @param request contiene los datos del nuevo registro de usuario
     */
    public function store($request)
    {
        $fullname         = $_POST["fullname"];
        $username         = $_POST["username"];
        $password         = $_POST["password"];
        $confirmpassword = $_POST["confirmpassword"];
      

        $message = new Message();
        $message = $this->message;
        $userModel = new User($this->config);
        $result = $userModel->userExists($username);
         // Verificar que todos los inputs estén llenos
         if ((ltrim($fullname) == "") || (ltrim($username) == "") || (ltrim($password) == ""))
         {
             $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
             view("users/new.php", compact("message"));
             exit;
         }

         // Verifico si el usuario ya existe
         if ($result["exists"] == 1)
         {
             $message->setWarningMessage(null, "Nombre de usuario ya existe", null, true);
             view("users/new.php", compact("message"));
             exit;
         }
         
         // Verifico si las contraseñas coinciden
         if ($password != $confirmpassword)
         {
             $message->setWarningMessage(null, "Las contraseñas no coinciden", null, true);
             view("users/new.php", compact("message"));
             exit;
         }
        $userModel->insertUser($request);
        $this->message->setSuccessMessage(null, "El registro se agregó correctamente", null, true);
       
         view("users/new.php", compact("message"));
      
    }

     /**
         * Muestra para vista "show" con los detalles del registro "id"
         * @param id del registro del cual se deben mostrar los detalles
         */
        public function show($id){
            $userModel = new User($this->config);
            $user = $userModel->getUser($id);
            $dataToView = ["login" => $this->login,
                           "user" => $user ];
            return view("users/show.php", $dataToView);
        }

        /**
        * Muestra para vista "edit" con los detalles del registro "id"
        * @param id del registro del cual se deben cargar los detalles para edición
        */
        public function edit($id){
            $userModel = new User($this->config);
            $user = $userModel->getUser($id);
            $dataToView = ["login" => $this->login,
                           "user" => $user ];
            return view("users/edit.php", $dataToView);
        }

        /**
        * Actualiza un registro de "user" en la base de datos
        * @param request contiene los datos del registro de usuario a actualizar
        */
        public function update($request)
        {
            $userModel = new User($this->config);
            $userModel->updateUser($request);
            $this->message->setSuccessMessage(null, "Se actualizo el registro correctamente", null, true);
            $this->index();
        }

        /**
         * Elimina un registro de la base de datos
         * @param id del registro que se desea eliminar
         */
        public function destroy($id)
        {
            $userModel = new User($this->config);
            $userModel->deleteUser($id);
            $this->message->setSuccessMessage(null, "Se eliminó el registro correctamente", null, true);
            $this->index();
          
            //header('Location: /users');
        }

    
}
}