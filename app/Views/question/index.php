<?php
include VIEWS . '/partials/header.php';
?>


<style type = 'text/css'>
h3 {
    font-size:150%;
    font-variant: small-caps;
    color:red;
}
</style>
<?php include VIEWS.'/partials/message.php' ?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>
<div class="container"><br>
  <div class="row">
    <div class="col-sm-12">
      <h3>Lista de Preguntas</h3>
      <br>
      <table class="table  is-fullwidth">
        <thead>
          <tr>
            <th class="text-center">Ver</th>
            <th class="text-center">Editar</th>
            <th class="text-center">Eliminar</th>
           
            <th scope="col">Titulo de Cuestionario</th>
            <th scope="col">Pregunta</th>
        </thead>
        <tbody>
          <?php foreach ($collection as $item) : ?>
            <tr>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-eye" href="<?= "/question/index.php?show=" . $item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-edit" href="<?= "/question/index.php?edit=" . $item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/question/index.php?delete=" . $item['id']; ?>"></a>
              </td>
           
              
              <td><?= $item['description']; ?></td>
              <td><?= $item['question_text']; ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
     
        <a class="button is-warning is-outlined" href="/question/index.php?action=new">Agregar Preguntas</a>
        <a class="button is-warning is-outlined" href="/index.php">Regresar</a>
      

    </div>
  </div>
</div>