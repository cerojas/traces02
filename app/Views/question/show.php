<?php include VIEWS . '/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>
<style type='text/css'>
  h2 {
    font-size: 150%;
    font-variant: small-caps;
  }
</style>
<style type='text/css'>
  h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }
</style>
<div class="hero-body">
  <!--coloca el contenedor en el centro de la pantalla-->
  <div class="container">
    <div class="columns is-centered">
      <!--centra las columnas en la pagina-->
      <div class="column is-6">
        <h3>Ver Preguntas</h3>
        <br>
        <br>

        <div class='control'>
          <label class='label'>Pregunta</label>
          <input class='input is-info' type='text' placeholder='Pregunta' name="question_text" value="<?php echo $question["question_text"]; ?>">
        </div>
        <br>
        <a class="button is-warning is-outlined" href="/question/index.php">Regresar</a>
        <a class="button is-warning is-outlined"  href=<?= "/question/index.php?edit=" . $question["id"] ?>>Editar </a>
      </div>
     
    

    </div>
   
  </div>

</div>


</div>
</div>


</body>

</html>