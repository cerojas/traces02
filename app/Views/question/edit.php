<?php include VIEWS . '/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>
<style type='text/css'>
  h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }
</style>
<div class="hero-body">
  <!--coloca el contenedor en el centro de la pantalla-->
  <div class="container">
    <div class="columns is-centered">
      <!--centra las columnas en la pagina-->
      <div class="column is-6">
        <h3>Editar Cuestionario</h3>
        <br>
        <br>
        <input type="hidden" id="view" value="edit">
        <form action="/question/index.php?action=update" method="post">
          <input type="hidden" name="id" value="<?= $question["id"]; ?>">
            <div class='field'>
          <div class='control'>
            <label class='label'>Pregunta</label>
            <input class='input is-info' type='text' placeholder='Pregunta' name="question_text"  value="<?php echo $question["question_text"]; ?>">
          </div>
          <br>
          <button type="submit" class="button is-success is-outlined">Guardar</button>

          <a class="button is-warning is-outlined" href="/question/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
</div>