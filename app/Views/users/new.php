<?php 
  include VIEWS.'/partials/header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
    <link rel="stylesheet" href="/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="layout-default">
<!--metodo para imprimir las alertas-->

<?php if($login=NULL) :?>

<nav class="navbar is-primary" role="navigation" aria-label="main navigation">

</nav>

<?php else : ?>

<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
<div class="navbar-end">
      <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
         <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1> 
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning"  value="Cerrar sesión"></a>
          <?php else : ?>

         
          <li class="navbar-divider">
 
           <a href="/authenticate/index.php?action=login"><input class="button is-link"  value="Iniciar sesión"></a> 
          </li>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</nav>

</div>
<?php include VIEWS.'/partials/message.php' ?>
    
        <div class="hero-body"><!--coloca el contenedor en el centro de la pantalla-->
          <div class="container">
            <div class="columns is-centered"> <!--centra las columnas en la pagina-->
              <article class="card is-rounde"> <!--contendor blanco que rodea los inputs-->
                <div class="card-content"><!--Ajusta el contedor blanco con margenes-->
                  <h1 class="title">
                
                    <img src= "/assets/img/logo.png" alt="logo" width="200">
                  </h1>
                

                  <form action="/users/index.php?action=save" method="post">
                  <div class="field">
                 
                    <div class="control "> <!--con esta clase le damos el espacio para la imagen en el input-->
                      <input class="input is-primary" type="text" placeholder="Nombre completo" id="fullname" name="fullname"><!--le damos color al input-->
                    </div>
                  </div>
                  <div class="field">
                 
                    <div class="control "> <!--con esta clase le damos el espacio para la imagen en el input-->
                      <input class="input is-primary" type="text" placeholder="Usuario" id="username" name="username"><!--le damos color al input-->
                    </div>
                  </div>
                  <div class="field"> <!--clase para los inputs-->
                    <div class="control "> <!--con esta clase le damos el espacio para la imagen en el input-->
                      <input class="input is-primary" type="password" placeholder="Contraseña" id="password" name="password"><!--le damos color al input-->
                    </div>
                  </div>
                  <div class="field"> <!--clase para los inputs-->
                    <div class="control "> <!--con esta clase le damos el espacio para la imagen en el input-->
                      <input class="input is-primary" type="password" placeholder="Confirmar Contraseña" id="confirmpassword" name="confirmpassword"><!--le damos color al input-->
                    </div>
                  </div>

                 <!-- <div class="field">
          <label for="role">Rol  </label>
          
        
  <div class="control">

    <div class="select is-info" >
      <select id="role" name="role" >
      <option value="R">Regular</option>
              <option value="S">Superusuario</option>
      </select>
    </div>
  </div>
</div>    

<div class="field is-6">
<label for="role">Estado : </label>
<div class="control is-6">
 
    <div class="select is-info is-6" >
   
      <select id="blocked" name="blocked"  >
      <option value="N">Desbloqueado</option>
              <option value="Y">Bloqueado</option>
      </select>
    </div>
  </div>
</div> -->
                 
                  <div class="field"> <!--clase para los inputs-->
                         <input type="submit" value="Registrar" id="register" class="button  is-primary is-medium " >
                        <a id="link"href="/index.php"> <input type="button" value="Regresar" id="cancel" class="button  is-danger is-medium " ></a>
                       
                     
                        </form>
              
                  </div>
                </div>
              </article>
            </div>
          </div>
    </div>

</body>
</html>

<?php endif; ?>