<?php include VIEWS.'/partials/header.php';
       ?>

<nav class="navbar is-primary" role="navigation" aria-label="main navigation">

<div class="navbar-end">
      <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
         <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1> 
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning"  value="Cerrar sesión"></a>
          <?php else : ?>
          <li class="navbar-divider">
 
           <a href="/authenticate/index.php?action=login"><input class="button is-link"  value="Iniciar sesión"></a> 
          </li>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</nav>
<style type = 'text/css'>
h3 {
    font-size:150%;
    font-variant: small-caps;
    color:red;
}
</style>

<div class="hero-body"><!--coloca el contenedor en el centro de la pantalla-->
          <div class="container">
            <div class="columns is-centered"> <!--centra las columnas en la pagina-->


 
<div class="column is-6">
<h3>Editar Usuario</h3>
<br>
 <br>
     

  
        
          <input type="hidden" id="view" value="edit">
          <form action="/users/index.php?action=update" method="post">
          <input type="hidden" name="id" value="<?= $user["id"]; ?>">
          
          <div class="field is-4">
  <div class="control ">
  <label class="label">Nombre Completo</label>
    <input class="input is-info" type="text" placeholder="Nombre Completo" name="fullname"  value="<?php echo $user["fullname"]; ?>" >
  </div>
</div>
<div class="field">
  <div class="control">
  <label class="label">Usuario</label>
    <input class="input is-info" type="text" placeholder="Usuario" name="username" value="<?php echo $user["username"]; ?>">
  </div>
</div>

<div class="field">
  <div class="control">
  <label for="password">Nueva Contraseña</label>
            <input 
              type="password" class="input is-info" id="password" name="password">
          </div>
          </div>
          <div class="form-check text-right">
            <input class="form-check-input" type="checkbox" value="" id="password" onclick="mostrarContrasena()">
            <label class="form-check-label" for="s">
              Mostrar contraseña
            </label>
          </div>   
          

          <div class="field">
          <label for="role">Rol  </label>
          
        
  <div class="control">

    <div class="select is-info" >
      <select id="role" name="role" >
              <option value="R" <?= $user["role"] == 'R' ? 'selected' : '' ?>>Regular</option>
              <option   value="S" <?= $user["role"] == 'S' ? 'selected' : '' ?>>Superusuario</option>
      </select>
    </div>
  </div>
</div>    

<div class="field is-6">
<label for="role">Estado : </label>
<div class="control is-6">
 
    <div class="select is-info is-6" >
   
      <select id="blocked" name="blocked"  >
      <option  value="N" <?= $user["blocked"] == 'N' ? 'selected' : '' ?>>Desbloqueado</option>
      <option  value="Y" <?= $user["blocked"] == 'Y' ? 'selected' : '' ?>>Bloqueado</option>
      </select>
    </div>
  </div>
</div> 
          
          <button type="submit" class="button is-success is-outlined">Guardar</button>
         
          <a class="button is-warning is-outlined" href="/users/index.php">Regresar</a>
          </form>
      </div>
    </div>
  </div>
  </div>
  
  
  <script>
  function mostrarContrasena(){
      var tipo = document.getElementById("password");
      if(tipo.type == "password"){
          tipo.type = "text";
      }else{
          tipo.type = "password";
      }
  }
</script>


