<?php 
  include VIEWS.'/partials/header.php';
   include VIEWS.'/partials/message.php' 
  

?>

<style type = 'text/css'>
h3 {
    font-size:150%;
    font-variant: small-caps;
    color:red;
}
</style>


<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
<div class="navbar-end">
      <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
         <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1> 
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning"  value="Cerrar sesión"></a>
        
          <?php else : ?>
          <li class="navbar-divider">
 
           <a href="/authenticate/index.php?action=login"><input class="button is-link"  value="Iniciar sesión"></a> 
          </li>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</nav>
  <div class="container"><br>
    <div class="row">
      <div class="col-sm-12">
        <h3>Lista de Usuarios</h3>
        <br>
        <table class="table  is-fullwidth">
          <thead>
            <tr>
              <th class="text-center">Ver</th>
              <th class="text-center">Editar</th>
              <th class="text-center">Eliminar</th>
              <th scope="col">Nombre Completo</th>
              <th scope="col">Usuario</th>
        
           <th scope="col">Rol</th>
           <th scope="col">Estado</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($collection as $item): ?>
            <tr>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-eye" href="<?= "/users/index.php?show=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-edit" href="<?= "/users/index.php?edit=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/users/index.php?delete=".$item['id']; ?>"></a>
              </td>
            
              <td><?= $item['fullname']; ?></td>
              <td><?= $item['username']; ?></td>
           
              <td> <?= $item['role']; ?></td>
              <td> <?= $item['blocked']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <a  href="/users/index.php?action=new"><input class= "button is-success is-outlined" value="Agregar Usuario"></a>
        <a href="/index.php"><input class="button is-link is-outlined"  value="Regresar"></a>
      </div>
    </div>
  </div>
  
