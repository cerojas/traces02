<?php
include VIEWS . '/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>

<?php include VIEWS.'/partials/message.php' ?>

<style type='text/css'>

h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }
</style>
<div class="container"><br>
  <div class="row">
    <div class="col-sm-12">
      <h3>Lista Resultados</h3>
      <table class="table  is-fullwidth">
        <thead>
          <tr>
            <th class="text-center">Ver</th>
            <th class="text-center">Editar</th>
            <th class="text-center">Eliminar</th>
            <th scope="col">Cuestionario</th>
            <th scope="col">Valor Minimo</th>
            <th scope="col">Valor Maximo</th>
            <th scope="col">Retroalimentación</th>
        </thead>
        <tbody>
          <?php foreach ($collection as $item) : ?>
            <tr>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-eye" href="<?= "/result/index.php?show=" . $item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-edit" href="<?= "/result/index.php?edit=" . $item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/result/index.php?delete=" . $item['id']; ?>"></a>
              </td>
              <td><?= $item['description']; ?>
              <td><?= $item['min_value']; ?></td>
              <td><?= $item['max_value']; ?></td>
              <td><?= $item['feedback']; ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
     
        <a class="button is-warning is-outlined" href="/result/index.php?action=new">Agregar Resultados</a>
        <a class="button is-warning is-outlined" href="/index.php">Regresar</a>
      

    </div>
  </div>
</div>