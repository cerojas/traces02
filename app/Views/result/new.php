<?php include VIEWS . '/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>

      <form action='/result/index.php?action=save' method='post'>
        <input type="hidden" name="id_user">
    
<style type='text/css'>

h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }
</style>

<?php include VIEWS.'/partials/message.php' ?>

<div class='hero-body'>
  <!--coloca el contenedor en el centro de la pantalla-->
  <div class='container'>
    <div class='columns is-centered'>
      <!--centra las columnas en la pagina-->
      <div class='column is-6'>
        <h3>Agregar Resultados</h3>
        <br>
        <br>
        <div class="field">
          <label for="questionnaire_id">Cuestionario</label>
          <div class="control">
            <div class="select is-info">
              <select id="questionnaire_id" name="questionnaire_id">
                <?php foreach ($questionnaire as $item2) : ?>
                  <option value="<?php echo $item2['id']; ?>"> <?php echo $item2['description']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>

      
        <div class='field'>
          <div class='control'>
            <label class='label'>Insertar Minimo</label>
            <input class='input is-info' type='text' placeholder='Insertar Minimo' name="min_value">
          </div>
          <div class='field'>
          <div class='control'>
            <label class='label'>Insertar Maximo</label>
            <input class='input is-info' type='text' placeholder='Insertar Maximo' name="max_value">
          </div>
          <div class='field'>
          <div class='control'>
            <label class='label'>Retroalimentación</label>
            <input class='input is-info' type='text' placeholder='Retroalimentación' name="feedback">
          </div>
          <div class='field'>
            <!--clase para los inputs-->
            <br>
            <input type='submit' value='Guardar' id='guardar' class='button  is-primary is-outlined'>
            <a class="button  is-primary is-outlined" href="/result/index.php">Regresar</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>
</div>
</div>
</body>
</html>