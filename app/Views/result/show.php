<?php include VIEWS . '/partials/header.php';
?>

<nav class="navbar is-primary" role="navigation" aria-label="main navigation">

  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">

            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>
<style type='text/css'>
  h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }
</style>

<div class="hero-body">
  <!--coloca el contenedor en el centro de la pantalla-->
  <div class="container">
    <div class="columns is-centered">
      <!--centra las columnas en la pagina-->



      <div class="column is-6">
        <h3>Ver Resultado</h3>
        <br>
        <br>


        <input type="hidden" id="view" value="edit">
       
          <input type="hidden" name="id" value="<?= $result["id"]; ?>">

          <div class="field is-4">
            <div class="control ">
              <label class="label">Insertar Minimo</label>
              <input class="input is-info" type="text" placeholder="Insertar Minimo" name="questionnaire_id" value="<?php echo $result["questionnaire_id"];?> " readonly>
            </div>
          </div>

          <div class="field is-4">
            <div class="control ">
              <label class="label">Insertar Minimo</label>
              <input class="input is-info" type="text" placeholder="Insertar Minimo" name="min_value" value="<?php echo $result["min_value"]; ?>"readonly>
            </div>
          </div>
          <div class="field">
            <div class="control">
              <label class="label">Insertar Maximo</label>
              <input class="input is-info" type="text" placeholder="Insertar Maximo" name="max_value" value="<?php echo $result["max_value"]; ?>"readonly>
            </div>
          </div>
          <div class="field">
            <div class="control">
              <label class="label">Retroalimentación</label>
              <input class="input is-info" type="text" placeholder="Retroalimentación" name="feedback" value="<?php echo $result["feedback"]; ?>" readonly>
            </div>
          </div>


          
          <a class="button is-warning is-outlined"  href=<?= "/result/index.php?edit=" . $result["id"] ?>>Editar </a>
          <a class="button is-warning is-outlined" href="/result/index.php">Regresar</a>
       
      </div>
    </div>
  </div>
</div>