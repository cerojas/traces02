<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel = 'stylesheet' href = '/assets/css/bulma.min.css'>
    <style type = 'text/css'>
h2 {
    font-size:150%;
    font-variant: small-caps;
}
</style>
</head>
<body>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item">
    <h2 >AvanSoft</h2>
    
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
  <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Cuestionarios
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item " href="views/listacuestionarios.php">
            Ver lista de cuestionarios para responder
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            Ver mis resultados
          </a>
          
        </div>
      </div>
    </div>
    <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Catalogos
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            Mantenimiento de usuarios
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            Mantenimiento de cuestionarios
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item"  href="views/listacuestionarios.php" >
          Mantenimiento de preguntas
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
          Mantenimiento de respuestas
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
          Mantenimiento de resultados
          </a>
         
        </div>
      </div>
    </div>
  

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          Reportes
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            Reporte por empleado
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            Reporte por cuestionario
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item"   >
            Grafico Top 10
          </a>
         
        </div>
      </div>
    </div>

    <div class="navbar-end">
  
      <div class="navbar-item">
        <div class="field is-grouped">
          <p class="control">
          <a class="dropdown-item" href="/authenticate/index.php?action=logout.php" class="button  is-danger is-medium " >Cerrar sesión</a>
          
          </p>
         </form>
        </div>

        <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <?php if (!is_null($login)) : ?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?= $_SESSION['login']['username'] ?>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/authenticate/index.php?action=logout.php">Cerrar sesión</a>
          </div>
        </li>
        <?php else : ?>
          <li class="nav-item">
            <a class="nav-link" href="/authenticate/index.php?action=login">Iniciar sesión</a>
          </li>
            <?php endif; ?>
      </li>
    </ul>

    
</nav>



<section class = 'hero  is-medium Light title is-bold'><!--coloca el contenedor en el centro de la pantalla-->
    <div class = 'hero-body'><!--coloca el contenedor en el centro de la pantalla-->
      <div class = 'container'>
        <div class = 'columns is-centered'> <!--centra las columnas en la pagina-->
              <h1>Gestion de su aplicacion de cuestionarios</h1>
        </div>
        <div class = 'columns is-centered'> <!--centra las columnas en la pagina-->
              <img src = '/assets/img/checklist03.png' alt = 'logo' width = '30%'>
        </div>
      </div>
    </div>

  </section>

</body>
</html>