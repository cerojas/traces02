<?php include VIEWS . '/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>
<style type='text/css'>
  h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }
</style>
<div class="hero-body">
  <!--coloca el contenedor en el centro de la pantalla-->
  <div class="container">
    <div class="columns is-centered">
      <!--centra las columnas en la pagina-->
      <div class="column is-6">
        <h3>Editar Respuestas</h3>
        <br>
        <br>
        <input type="hidden" id="view" value="edit">
        <form action="/answer/index.php?action=update" method="post">
          <input type="hidden" name="id" value="<?= $answer["id"]; ?>">
          <div class='control'>
          <label class='label'>Pregunta</label>
          <input class='input is-info' type='text' placeholder='Pregunta' name="question_text" value=" <?php 
        
        $v1 = $_POST['question_text'];
        echo $v1;
                 
                  ?>
          " readonly>
        </div>
        

         
        <div class='control'>
          <label class='label'>Numero de Respuesta</label>
          <input class='input is-info' type='text' placeholder='Pregunta' name="number" value="<?php echo $answer["number"];?> " >
        </div>
     
     

        <div class='control'>
          <label class='label'>Respuesta</label>
          <input class='input is-info' type='text' placeholder='Pregunta' name="answer_text" value="<?php echo $answer["answer_text"]; ?>" >
        </div>
        <div class='control'>
          <label class='label'>Puntos</label>
          <input class='input is-info' type='text' placeholder='Pregunta' name="answer_points" value="<?php echo $answer["answer_points"]; ?>" >
        </div>
          <br>
          <button type="submit" class="button is-success is-outlined">Guardar</button>

          <a class="button is-warning is-outlined" href="/answer/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
</div>