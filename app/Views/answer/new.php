<?php include VIEWS . '/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>

<?php include VIEWS.'/partials/message.php' ?>

      <form action='/answer/index.php?action=save' method='post'>
        <input type="hidden" name="id_user">
  

<style type = 'text/css'>
h3 {
    font-size:150%;
    font-variant: small-caps;
    color:red;
}
</style>
<div class='hero-body'>
  <!--coloca el contenedor en el centro de la pantalla-->
  <div class='container'>
    <div class='columns is-centered'>
      <!--centra las columnas en la pagina-->
      <div class='column is-6'>
        <h3>Agregar Respuesta</h3>
        <br>
        <br>
        <div class="field">
          <label for="question_id">Preguntas </label>
          <div class="control">
            <div class="select is-info">
              <select id="question_id" name="question_id">
                <?php foreach ($question as $item2) : ?>
                  <option value="<?php echo $item2['id']; ?>"> <?php echo $item2['question_text']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>

      
        <div class='field'>
          <div class='control'>
            <label class='label'>Insertar Respuesta</label>
            <input class='input is-info' type='text' placeholder='Insertar Pregunta' name="answer_text">
          </div>
        

          <div class='field'>
          <div class='control'>
            <label class='label'>Insertar Numero</label>
            <input class='input is-info' type='text' placeholder='Insertar Numero' name="number">
          </div>

          <div class='field'>
          <div class='control'>
            <label class='label'>Insertar Puntos</label>
            <input class='input is-info' type='text' placeholder='Insertar Respuesta' name="answer_points">
          </div>
        
            <br>
            <br>
            <!--clase para los inputs-->
            <input type='submit' value='Guardar' id='guardar' class='button  is-primary is-outlined'>
            <a class='button  is-primary is-outlined' href="/answer/index.php">Regresar</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>
</div>
</div>
</body>
</html>