<?php

include VIEWS . '/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>

<style type = 'text/css'>
h3 {
    font-size:150%;
    font-variant: small-caps;
    color:red;
}
</style>


<?php include VIEWS.'/partials/message.php' ?>

<div class="container"><br>
  <div class="row">
    <div class="col-sm-12">
      <h3>Lista de Respuestas</h3>
      <br>

      <table class="table  is-fullwidth">
        <thead>
          <tr>
            <th class="text-center">Ver</th>
            <th class="text-center">Editar</th>
            <th class="text-center">Eliminar</th>
           <!-- <th scope="col">Id</th> -->
           <!-- <th scope="col">Id_Question</th>-->
            <th scope="col">Pregunta</th>
          
            <th scope="col">Respuesta</th>
            <th scope="col">Puntos</th>
        </thead>
      
       
        <tbody>
        
        <?php array_map(function ($v1,$v2){
            ?>
            
        
            <tr>
          
              <td class="text-center">
              <form action="<?= "/answer/index.php?show=" . $v2['id']; ?>" method="post">
                
                <input type="hidden" name="question_text" value="<?= $v1['question_text']; ?>">
                <input type="submit" href="/answer/index.php?show" value="Ver" class= "button is-small is-success" >
                </form>
               
               
                
                
               
              </td>
              <td class="text-center">
              <form action="<?= "/answer/index.php?edit=" . $v2['id']; ?>" method="post">
              <input type="hidden" name="question_text" value="<?= $v1['question_text']; ?>">
                <input type="submit" href="/answer/index.php?edit" value="Editar" class= "button is-small is-success" >
                </form>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/answer/index.php?delete=" . $v2['id']; ?>"></a>
              </td>
             
           
              <!-- <td><?= $v2['id']; ?><td>-->
              <!-- <td><?= $v2['question_id']; ?></td>-->



              <td><?= $v1['question_text']; ?></td>
          
        
             
              <td><?= $v2['answer_text']; ?></td>
              <td><?= $v2['answer_points']; ?></td>
         
        <?php }, $question,$collection);  ?>
      
             
            
              </tr>
        </tbody>

     
       
      </table>


     
        <a class="button is-warning is-outlined" href="/answer/index.php?action=new">Agregar Respuestas</a>
        <a class="button is-warning is-outlined" href="/index.php">Regresar</a>
      

    </div>
  </div>
</div>
