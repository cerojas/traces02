<?php include VIEWS.'/partials/header.php';
      ?>

<nav class="navbar is-primary" role="navigation" aria-label="main navigation">

<div class="navbar-end">
      <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
         <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1> 
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning"  value="Cerrar sesión"></a>
          <?php else : ?>
          <li class="navbar-divider">
 
           <a href="/authenticate/index.php?action=login"><input class="button is-link"  value="Iniciar sesión"></a> 
          </li>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</nav>

<style type = 'text/css'>
h2 {
    font-size:150%;
    font-variant: small-caps;
}
</style>
    <style type = 'text/css'>
h3 {
    font-size:150%;
    font-variant: small-caps;
    color:red;
}
</style>
<div class="hero-body"><!--coloca el contenedor en el centro de la pantalla-->
          <div class="container">
            <div class="columns is-centered"> <!--centra las columnas en la pagina-->


 
<div class="column is-6">
<h3>Ver Cuestionarios</h3>

<br>
 <br>
    <div class="field is-4">
  <div class="control ">
  <label class="label">Titulo del cuestionario</label>
    <input class="input is-info" type="text" placeholder="Titulo del cuestionario"   value="<?php echo $questionnaire["description"]; ?>" readonly>
  </div>
</div>

<div class="field">
  <div class="control">
  <label class="label">Descripcion del Cuestionario</label>
    <input class="input is-info" type="text" placeholder="Descripcion del Cuestionario" value="<?php echo $questionnaire["long_description"]; ?>" readonly>
  </div>
</div>


  <a id="link"href=<?= "/questionnaire/index.php?edit=".$questionnaire["id"]?>> 

  <button class="button  is-warning is-outlined">
    <span class="icon">
      <i class="zmdi zmdi-edit zmdi-hc-lg"></i>
    </span>
    <span>Editar</span>
  </button>
  </a>

  <a id="link"href="/questionnaire/index.php"> 

  <button class="button is-success is-outlined">
    <span class="icon">
      <i class="zmdi zmdi-long-arrow-return"></i>
    </span>
    <span>Regresar</span>
  </button>
  </a>


</div>
</div>
</div>


          </div>
    </div>
    
                 
</body>
</html>