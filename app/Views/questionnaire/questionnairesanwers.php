<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cuestionario de Amistad</title>
    <!--framework css bulma-->
   <link rel="stylesheet" href="/assets/css/bulma.min.css">
    <!--  <link href="/assets/css/bootstrap.min.css" rel="stylesheet"> -->
    <!--libreria de javascript-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
 


   <h1 style = "color: red; font-size:200%"> <?php echo $questionnaire["description"]; ?></h1>

  

     
   <br>

    <form action='/questionnaire/index.php?action=result' method='post'>
   <div class="container">
  
   <?php

    include "../conexion.php";
    $id = $_GET['quest'];
   // echo $id;
    
    //selecciona las preguntas
    $questions = $mysqli->query("SELECT * FROM questions where questionnaire_id=" . $id);

    // recorre las preguntas
    while ($question = mysqli_fetch_array($questions)) {
      //selecciona las respuestas
      $answers = $mysqli->query("SELECT * FROM answers where question_id=" . $question['id'] . " order by number");
      ?>
               
          
   
   
              

<!--imprimo las preguntas-->
  

      <div class="row" >
      <br>
     
        <div class="">
       
          <div class="card">
         
            <div class="card-content">

            <?php   echo $question['question_text'];     ?>
                    
     
                  
          
            
            </div>
           
         
                 <div class="card-content">
               
                 <?php
                //recorre las respuestas
                  while ($answer = mysqli_fetch_array($answers)) {

                    ?>
               
                
                  <!--lista las respuestas-->
                 <li style="list-style:none;"><input  id="sel" name="sel" type="checkbox"  value="<?php echo $answer['answer_points'] ?>" > <?php echo "&nbsp;"; echo "&nbsp;";  echo $answer['answer_text'] ?> </li>
              
                 <?php
                  }
                  ?>

            </div>
           
     
       
          </div>
      
        </div>
     
      </div>
    

   
      <?php
                  }
                  ?>
      
   
    
 
    <br>
    <div class="row">
      <div class="col-md-12">
        <input type="hidden" id="totalSum" name="totalSum">
        <input type="hidden" id="questionnaryid" name="questionnaryid" value="<?php echo $id ?>">
        <button type="submit" class="button is-primary">Ir al resultado </button>
       
      </div>
    </div>
  </div>
</form>
</body>
</html>

<!--codigo de javascript que suma los puntos-->
<script>
  $(document).ready(function() {
    $("input[type=checkbox]").click(function() {
    var total = 0;
    $("input[type=checkbox]:checked").each(function() {
        total += parseFloat($(this).val());
    });

    $("#totalSum").val(total);
});
});
</script>
