<?php 
  include VIEWS.'/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
            
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>
<style type='text/css'>

h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }
</style>

<?php
// Conexion a la base de datos 
include "../conexion.php";

// Validar si ya contestó el test seleccionado




$validacion = $mysqli->query("SELECT * FROM users_questionnaires INNER JOIN questionnaires ON users_questionnaires.questionnaire_id=questionnaires.id AND user_id=". $_SESSION['login']['id']);
 
//var_dump($validacion);
?>

<?php
//seleccionar los datos de la base de datos
$sql = $mysqli->query("SELECT users.id, users.username, questionnaires.description, results.feedback,users_questionnaires.questionnaire_id,
                      users_questionnaires.result, results.min_value, results.max_value
                      FROM ((users_questionnaires
                      INNER JOIN  users ON users.id=users_questionnaires.user_id
                      INNER JOIN  questionnaires ON questionnaires.id = users_questionnaires.questionnaire_id
                      INNER JOIN  results ON questionnaires.id = results.questionnaire_id))
                      where users.id=" . $_SESSION['login']['id'] . "
                      AND users_questionnaires.questionnaire_id= questionnaires.id
                      and results.min_value <= users_questionnaires.result 
                      and results.max_value >= users_questionnaires.result");

      if ($validacion==true)  : ?>


  <div class="container"><br>
    <div class="row">
      <div class="col-sm-12">
        <h3>Ver mis Resultados</h3>
        <table class="table  is-fullwidth">
          <thead>
            <tr>
              <th class="text-center">Cuestionario</th>
              <th class="text-center">Puntos</th>
              <th class="text-center">Resultado</th>
            
          </thead>
          <tbody>
          <?php foreach ($sql as $opcion) : ?>
            <tr>
              <td><?= $opcion['description']; ?></td>
              <td><?= $opcion['result']; ?></td>
              <td><?= $opcion['feedback']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
       

        <a class="button is-warning is-outlined" href="/index.php">Regresar</a>
      
      </div>
    </div>
  </div>
  <?php
    else : ?>
    <h3><?php echo "No hay resultados para este usuario"; ?> </h3> 
<?php endif; ?>



