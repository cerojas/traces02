
<!-- Instancia el header--> 
<?php 
  include VIEWS.'/partials/header.php';
?>
<!-- Navbar que mantiene el usuario logueado--> 
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>
  <div class="container"><br>
    <div class="row">
      <div class="col-sm-12">
        <h3>Lista Cuestionarios</h3>

    
    <!-- Aplicar estilo a los titulos h2 y h3-->     


        <style type='text/css'>
  h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }

  h2 {
    font-size: 150%;
    font-variant: small-caps;
    color: green;
  }
</style>
       
        <?php 
        //conexion a la base de datos
        include "../conexion.php";
       $id= $_SESSION['login']['id'];
       //echo $id;


        //validacion para saber si el usuario ya contesto el cuestionario
        $validacion2 = $mysqli->query("SELECT * FROM users_questionnaires INNER JOIN questionnaires WHERE users_questionnaires.questionnaire_id=questionnaires.id and user_id=" . $_SESSION['login']['id']);
        $validacion = $mysqli->query("SELECT *
        FROM questionnaires
        WHERE NOT EXISTS (SELECT *
                          FROM users_questionnaires
                          WHERE users_questionnaires.questionnaire_id = questionnaires.id AND users_questionnaires.user_id=$id)");
        
        //echo $_SESSION['login']['id'];

        //var_dump($validacion);

  
       
      // var_dump($validacion);
      
       if ($validacion2==TRUE) :?>

    
<table class="table  is-fullwidth">
          <thead>
            <tr>
              <th class="text-center">Ir a Cuestionario</th>
              <th scope="col">Titulo del Cuestonario</th>
              <th scope="col">Descripcion del Cuestionario</th>
          </thead>
          <tbody>
            <?php foreach ($validacion as $item): ?>
            <tr>
                <td class="text-center">
                  <a class="btn btn-sm btn-secondary fas fa-sign-in-alt ml-1" href="<?= "/questionnaire/index.php?quest=".$item['id']; ?>"></a>
                </td>
                <td><?= $item['description']; ?></td>
                <td><?= $item['long_description']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        
        <a class="button is-warning is-outlined" href="/index.php">Regresar</a>

        <?php
    else : ?>

        <?php if ($validacion2->num_rows === 0) :?>
         
     
          
           <!--Tabla si la validacion es false--> 

        <table class="table  is-fullwidth">
          <thead>
            <tr>
              <th class="text-center">Ir a Cuestionario</th>
              <th scope="col">Titulo del Cuestonario</th>
              <th scope="col">Descripcion del Cuestionario</th>
          </thead>
          <tbody>
            <?php foreach ($collection as $item): ?>
            <tr>
                <td class="text-center">
                  <a class="btn btn-sm btn-secondary fas fa-sign-in-alt ml-1" href="<?= "/questionnaire/index.php?quest=".$item['id']; ?>"></a>
                </td>
                <td><?= $item['description']; ?></td>
                <td><?= $item['long_description']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <?php endif; ?>
        <a class="button is-warning is-outlined" href="/index.php">Regresar</a>
      
      </div>
    </div>
  </div>
  <?php endif; ?>
 
  
 