<?php 
  include VIEWS.'/partials/header.php';
?>
<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
  
 
  
</nav>

 
  <head>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
  
 
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawChart);
	
	
	
	

    function drawChart() {
      // Define the chart to be drawn.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Element');
      data.addColumn('number', 'Porcentaje');
	  <?php foreach ($collection as $item): ?>
	  
	  
	  
	
	  
      data.addRows([ 
        ['<?= $item['fullname']?>', <?= $item['canquest']?>*100]
       
      ]);
	  
	   <?php endforeach; ?>
	   
	  var options = {
          title: 'Top 5 de Cuestionarios respondidos',
          is3D: true,
		   
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
  </script>
</head>
<body>
  <!-- Identify where the chart should be drawn. -->
  <div id="chart_div" style="width: 900px; height: 500px;"></div>



<a class="button is-warning is-outlined" href="/index.php">Regresar</a>

</body>
  