
<?php

include VIEWS . '/partials/header.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="field">
            <div class="control">
              <label class="label">Retroalimentación</label>
              
              <textarea name="feedback" class="textarea is-info" readonly><?php echo $questionnaire["feedback"]; ?></textarea>
            </div>
          </div>

</body>
</html>