<?php include VIEWS . '/partials/header.php';
?>

<nav class="navbar is-primary" role="navigation" aria-label="main navigation">

  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1>
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">

            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>
<style type='text/css'>
  h3 {
    font-size: 150%;
    font-variant: small-caps;
    color: red;
  }
</style>

<div class="hero-body">
  <!--coloca el contenedor en el centro de la pantalla-->
  <div class="container">
    <div class="columns is-centered">
      <!--centra las columnas en la pagina-->



      <div class="column is-6">
        <h3>Editar Cuestionario</h3>
        <br>
        <br>


        <input type="hidden" id="view" value="edit">
        <form action="/questionnaire/index.php?action=update" method="post">
          <input type="hidden" name="id" value="<?= $questionnaire["id"]; ?>">

          <div class="field is-4">
            <div class="control ">
              <label class="label">Titulo del Cuestionario</label>
              <input class="input is-info" type="text" placeholder="Titulo del Cuestionario" name="description" value="<?php echo $questionnaire["description"]; ?>">
            </div>
          </div>
          <div class="field">
            <div class="control">
              <label class="label">Descripcion del Cuestionario</label>
              <input class="input is-info" type="text" placeholder="Descripcion del Cuestionario" name="long_description" value="<?php echo $questionnaire["long_description"]; ?>">
            </div>
          </div>



          <button type="submit" class="button is-success is-outlined">Guardar</button>

          <a class="button is-warning is-outlined" href="/questionnaire/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
</div>