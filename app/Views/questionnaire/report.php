<?php 
  include VIEWS.'/partials/header.php';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="/assets/css/all.css">
</head>
<body>

<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
  <div class="navbar-end">
    <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
            <h5>Usuario : <?= $_SESSION['login']['username'] ?></h5>
            
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning" value="Cerrar sesión"></a>
        <?php else : ?>
          <li class="navbar-divider">
            <a href="/authenticate/index.php?action=login"><input class="button is-link" value="Iniciar sesión"></a>
          </li>
        <?php endif; ?>
        </div>
    </div>
  </div>
  </div>
</nav>

<?php 
    
   

?>
        <form action='/questionnaire/index.php?action=reportemployee' method='post'>

          <label for="id">Empleado : </label>
             <select id="id" name="id" class="form-control">
                <?php foreach ($collection as $item2) : ?>
                  <option value="<?php echo $item2['id']; ?>"> <?php echo $item2['fullname']; ?> </option>
                
             
                <?php endforeach; ?>
              </select>
              <br>
              

              <input type="submit" id="enviar" name="enviar" class="btn btn-outline-success btn-lg btn-block" value="Filtrar">

                </form>
            </div>
          </div>
        </div>


     

        <?php 


if (!isset($_REQUEST['enviar']))
{
  $_POST["id"]=0;
}else {
  echo $_POST["id"];
}

include "../conexion.php";

$reports = $mysqli->query(" SELECT qu.description , q.result , q.questionnaire_id
FROM questionnaires qu INNER JOIN users_questionnaires q ON qu.id=q.questionnaire_id AND q.user_id=" . $_POST["id"]);
?>
      

       
  
      
        <br>
<table class="table  is-fullwidth">
        <thead>
          <tr>
            <th class="text-center">Resultado</th>
            <th class="text-center">Borrar Intento</th>
            <th scope="text-center">Cuestionario</th>
            <th scope="col">Puntos</th>
            
        </thead>
        <tbody>
        <?php foreach ($reports as $item): ?>
            <tr>
             
              <td class="text-center">
                <a class="btn btn-sm btn-primary fas fa-eye " href="<?= "/result/index.php?edit=" . $item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm  btn-primary fas fa-trash" href="<?= "/result/index.php?delete=" . $item['id']; ?>"></a>
              </td>
              <td><?= $item['description']; ?></td>
              <td><?= $item['result']; ?></td>
           
            </tr>

            <?php endforeach; ?>
     
        </tbody>
      </table>

      <br>

<a class="button is-warning is-outlined" href="/index.php">Regresar</a>
</body>
</html>