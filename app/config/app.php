<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Parámetros generales
    |--------------------------------------------------------------------------
    | Definir en los parámetros generales de la aplicación
    */
    'debug'      => false,
    'public'     => "/",

    /*
    |--------------------------------------------------------------------------
    | Database parameters
    |--------------------------------------------------------------------------
    | En esta sección se deben definir los parámetros de conexión a BD
    */
    'server'       => 'localhost',
    'database'     => 'isw613_questionnaires',
    'user'         => 'root',
    'password'     => 'root',

    // Parámetros de base de datos

];
