<?php
//codigo de conexion a la base datos

//variables para la conexion de la base de datos
$dbHost     = '127.0.0.1';
$dbUser     = 'root'; // isw613_user
$dbPassword = 'root'; //secret
$dbName     = 'isw613_questionnaires';
$dbPort     = '3306';

// Realiza la conexión a BD
$mysqli = new mysqli( $dbHost, $dbUser, $dbPassword, $dbName, $dbPort );
if ( $mysqli->connect_errno ) {
    echo 'Error de conexión de MySQL: (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
}