<?php
    // Esta es el "dispatcher" de usuarios

    // En cada script hay que cargar el init.php
    include_once $_SERVER['DOCUMENT_ROOT']."/../app/init.php";

    // Se cargan las clases que se ocupan
    include_once CONTROLLERS."/QuestionnairesController.php";
    include_once MODELS."/Questionnaire.php";
    include_once MODELS."/Question.php";
    include_once MODELS."/Answer.php";
    include_once MODELS."/User.php";
   
    

    // Se hace el "use" de las clases que se utilizaran en este script
    use MyApp\Controllers\QuestionnairesController;
    $controller = new QuestionnairesController($config, $login);

    // Si viene asignado el parámetro "show" se muestra la vista "show"
    if (isset($_GET['show']))
        $controller->show($_GET['show']);

        elseif (isset($_GET['showTried']))
        $controller->showTried($_GET['showTried']);


        

    // Si viene asignado el parámetro "edit" se muestra la vista "edit"
    elseif (isset($_GET['edit']))
        $controller->edit($_GET['edit']);

        

    // Si viene asignado el parámetro "delete" se llama al método "destroy"
    elseif (isset($_GET['delete']))
        $controller->destroy($_GET['delete']);

        elseif (isset($_GET['deleteTried']))
        $controller->destroyTried($_GET['deleteTried']);

        elseif (isset($_GET['quest']))
        $controller->questionnaire($_GET['quest']);


        

    // Otras acciones se capturan por medio del parámetro "action"
    elseif (isset($_GET['action'])){
        switch ($_GET['action']) {
            case 'new':
                $controller->create();
                break;
            case 'save':
                $controller->store($_POST);
                break;
            case 'update':
                $controller->update($_POST);
                break;

                case 'lista':
                $controller->list();
                break;

                case 'result':
                $controller->result();
                break;

                case 'myresult':
                $controller->myresult();
                break;

                case 'reportemployee':
                $controller->reportemployee();
                break;

                case 'reportquestionnaire':
                $controller->reportquestionnaire();
                break;


                case 'graphic':
                $controller->graphic();
                break;

                
            default:
                break;
        }
    }

    // En cualquier otro caso se muestra la vista "index"
    else
        $controller->index();