<!DOCTYPE html>
<html>

  <head>
    <script data-require="jquery@*" data-semver="2.1.1" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script data-require="bootstrap@3.1.1" data-semver="3.1.1" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link data-require="bootstrap-css@3.1.1" data-semver="3.1.1" rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="style.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-mockjax/1.5.3/jquery.mockjax.js"></script>
    <script src="mockresponse.js"></script>
    <script src="script.js"></script>


  <style type = 'text/css'>
.carousel-container {
  width: 60%;
  position: absolute;
    top: 20%;
    left: 32%;
}
</style>




  </head>

  <body>
 
    <h1>Carousel - Ajax Load Test</h1>
    <div class="carousel-container">
    <div class="row justify-content-center">
    <div class="col-sm-8">
      <div id="myCarousel" class="carousel slide" data-interval="false">
          <!-- Indicators -->
          <ol class="carousel-indicators" id="indicators">
          </ol>
          <div class="carousel-inner" id="homepageItems">
          </div>
          <div class="carousel-controls"> 
            <a class="carousel-control left" href="#myCarousel" data-slide="prev"> 
              <span class="glyphicon glyphicon-chevron-left"></span> 
            </a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next"> 
              <span class="glyphicon glyphicon-chevron-right"></span> 
            </a>
          </div>
      </div>
    </div>
    </div>
</div>
  </body> 

</html>

<script>
$(document).ready(function() {
   $.ajax( {
       url: '/carousel',
       dataType: 'json',
       success: function(data) {
        var response = '',
            indicator = '';
        for (var i = 0; i < data.d.results.length; i++) {
            response += '<div class="item"><img src="' + data.d.results[i].Image_x0020_URL + '"><div class="carousel-caption"><h3>' + data.d.results[i].Title + '</h3><p>' + data.d.results[i].Content + '</p><p><a class="btn btn-info btn-sm">Read More</a></p></div></div>';
            indicator += '<li data-target="#myCarousel" data-slide-to="'+i+'"></li>';
        }
        $('#homepageItems').append(response);
        $('#indicators').append(indicator);
        $('.item').first().addClass('active');
        $('.carousel-indicators > li').first().addClass('active');
        $("#myCarousel").carousel();
       }
   });
});

</script>

<script>
var aResponse = { d: { results: [ {id: 1, Image_x0020_URL: "http://lorempixel.com/g/640/480/", Title: "Test A", Content: "Test Text AAAAAA"}, 
                                    {id: 2, Image_x0020_URL: "http://lorempixel.com/g/640/480/", Title: "Test B", Content: "Test Text BBBBBB"}, 
                                    {id: 3, Image_x0020_URL: "http://lorempixel.com/g/640/480/", Title: "Test C", Content: "Test Text CCCCCC"}, 
                                    {id: 4, Image_x0020_URL: "http://lorempixel.com/g/640/480/", Title: "Test D", Content: "Test Text DDDDDD"}, 
                                    {id: 5, Image_x0020_URL: "http://lorempixel.com/g/640/480/", Title: "Test E", Content: "Test Text EEEEEE"}, 
                                    {id: 6, Image_x0020_URL: "http://lorempixel.com/g/640/480/", Title: "Test F", Content: "Test Text FFFFFF"}, 
                                    {id: 7, Image_x0020_URL: "http://lorempixel.com/g/640/480/", Title: "Test G", Content: "Test Text GGGGGG"}, 
                                    {id: 8, Image_x0020_URL: "http://lorempixel.com/g/640/480/", Title: "Test H", Content: "Test Text HHHHHH"} 
                                  ] 
                        } 
};

$.mockjax({
    url:  '/carousel',
    dataType: 'json',
    responseText: aResponse
});

</script>
