
 <?php include "../config.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cuestionaries</title>
    <link rel="stylesheet" href="/css/bulma.min.css">
</head>
<body>
  <!--codigo para mostrar el navbar-->
<nav class="navbar is-dark">
  <div class="navbar-brand">
    <a class="navbar-item" >
     <span>AvanSoftware</span>
    </a>
    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
      
      <span></span>
      <span></span>
    </div>
  </div>

<!--codigo para ejecutar el formulario logout-->
  <div class="navbar-end">
  <form action="../actions/logout.php" method="POST">
      <div class="navbar-item">
        <div class="field is-grouped">
          <p class="control">

          <input type="submit" value="Logout" id="loginbutton" class="button  is-danger is-medium " >
          
          </p>
         </form>
        </div>
      </div>
    </div>
    <div class="navbar-end">
      <div class="navbar-end">
      <?php if (!is_null($login)) : ?>
        <div class="buttons">
          <a class="button is-primary">
         <h1>Usuario : <?= $_SESSION['login']['username'] ?></h1> 
          </a>
          <a href="/authenticate/index.php?action=logout.php"><input class="button is-warning"  value="Cerrar sesión"></a>
          <?php else : ?>
          <li class="navbar-divider">
 
           <a href="/authenticate/index.php?action=login"><input class="button is-link"  value="Iniciar sesión"></a> 
          </li>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
      
   </nav>
   <br>
   <br>
  
<!--codigo para mostrar el cuestionario con id 1-->
    <div class="box">

    
  <article class="media">

    <div class="media-left">
      <figure class="image is-64x64">
        <img src="/img/test1.jpg" alt="Image">
      </figure>
    </div>
    <div class="media-content">
      <div class="content">
        <p>
        <?php
//conexion a la base de datos
include "../actions/conexion.php";
//consulta e impresion de los datos seleccionados
 $consulta= $mysqli->query("SELECT*FROM questionnaires WHERE id=1");
 while ($fila =mysqli_fetch_array($consulta)){
   echo "<p>";
   echo $fila ["id"];
   echo "-";
   echo $fila["description"];
   echo "</br>";
   echo "</br>";
   echo $fila ["long_description"];
   echo "</p>";


?>    
<!--envio de id al formulario del cuestionario-->
         <a href="<?= $public ?>views/testTrabajo.php?id=<?= $fila['id'] ?>" class="button  is-success is-medium ">Empezar Test</a>

         <?php } ?>
</article>
</div>

<br>
<br>
<!--codigo para mostrar el cuestionario con id 2-->

<div class="box">
  <article class="media">
    <div class="media-left">
      <figure class="image is-64x64">
        <img src="/img/test2.png" alt="Image">
      </figure>
    </div>
    <div class="media-content">
    
      <div class="content">
      <?php
      //conexion a la base de datos
 include "../actions/conexion.php";
 //consulta e impresion de los datos seleccionados
 $consulta= $mysqli->query("SELECT*FROM questionnaires WHERE id=2");
 while ($fila =mysqli_fetch_array($consulta)){
   echo "<p>";
   echo $fila ["id"];
   echo "-";
   echo $fila["description"];
   echo "</br>";
   echo "</br>";
   echo $fila ["long_description"];
   echo "</p>";


?>
<!--envio de id al formulario del cuestionario-->
     <a href="<?= $public ?>views/testSalud.php?id=<?= $fila['id'] ?>" class="button  is-success is-medium ">Empezar Test</a>
     <?php } ?>
</article>
</div>

<br>
<br>

<!--codigo para mostrar el cuestionario con id 3-->
<div class="box">
  <article class="media">
    <div class="media-left">
      <figure class="image is-64x64">
        <img src="/img/test3.jpg" alt="Image">
      </figure>
    </div>
    <div class="media-content">
      <div class="content">
      <?php
      //conexion a la base de datos
      include "../actions/conexion.php";
      //consulta e impresion de los datos seleccionados
      $consulta= $mysqli->query("SELECT*FROM questionnaires WHERE id=3");
      while ($fila =mysqli_fetch_array($consulta)){
        echo "<p>";
        echo $fila ["id"];
        echo "-";
        echo $fila["description"];
        echo "</br>";
        echo "</br>";
        echo $fila ["long_description"];
        echo "</p>";
    
  
?>
<!--envio de id al formulario del cuestionario-->
          <a href="<?= $public ?>views/testAmistad.php?id=<?= $fila['id'] ?>" class="button  is-success is-medium ">Empezar Test</a>
          <?php } ?>
  </article>
</div>
</body>
</html>