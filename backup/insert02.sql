
//insert para las respuestas del test de trabajo
INSERT INTO answers VALUES  
(1,1,1,'Colabora si es necesario pero bajo la direccion de otra persona',15),
(2,1,2,'Si es posible evito participar en la toma de decisiones',10),
(3,1,3,'Me gusta enfrentarme al reto',20),

(4,2,1,'Me bloqueo e intento evitarlo por todos los medios',10),
(5,2,2,'Si es necesario lo hago pero no me entusiasma',15),
(6,2,3,'No tengo ningun problema, me gusta hablar en publico',20),

(7,3,1,'Intento pasar desapercibido y que no se note mi presencia',10),
(8,3,2,'Me encanta,poder escuchar el punto de vista y las opiniones de los demás',20),
(9,3,3,'No suelo estar cómodo pero salgo del paso con esfuerzo',15),

(10,4,1,'Prefiero escuchar y no dar mi opinion',10),
(11,4,2,'Me parece importante exponer mi punto de vista por si ayuda',20),
(12,4,3,'Solo hablo en el caso de que no me convenza la opinion de los demas',15),

(13,5,1,'Siempre me adapto a lo que decidan los demas',10),
(14,5,2,'Normalmente acabamos haciendo lo que yo prefiero',20),
(15,5,3,'Suelo opinar pero casi siempre se acaba haciendo lo que dicen los demas',15);





//insert para las preguntas del test de trabajo

INSERT INTO questions VALUES (1,1,'Si surge la necesidad de tener que organizar equipos...'),
(2,1,'A la hora de hablar en público....'),
(3,1,'Cuando participo en una reunión de más de 4 personas'),
(4,1,'A la hora de exponer tu punto de vista ante un grupo de personas ?'),
(5,1,'A la hora de organizar planes con lo amigos..');
//insert para las respuestas del test de salud

INSERT INTO questions VALUES
(6,2,"Tengo periodos en los que tiendo a hablar mucho más rápido de lo que habitualmente lo hago?"),
(7,2,"Mi autoestima varia entre tener grandes dudas sobre mi mismo hasta un exceso de confianza igual de grande?"),
(8,2,"He tenido períodos de gran optimismo y otros períodos de pesimismo igualmente grandes?"),
(9,2,"He tenido momentos en los que me he sentido exaltado y deprimido al mismo tiempo?"),
(10,2,"Tengo períodos de embotamiento mental y otros períodos de pensamiento muy creativo?");


//insert para las respuestas del test de Amistad
INSERT INTO questions VALUES
(11,3,"Un amigo traicionó tu confianza. ¿Qué haces?"),
(12,3,"Serias capaz de renunciar a algo importante por ayudar a un amigo en apuros?"),
(13,3,"Tienes el mismo nivel de confianza con todos tus amigos?"),
(14,3,"Si descubres que alguno de tus amigos esta enamorado de la misma persona que ti. entonces:"),
(15,3," Pasar una tarde charlando con un buen amigo, ¿es uno de los mejores placeres de la vida?");


//insert para las preguntas del test de Habilidades
INSERT INTO questions VALUES
(16,4,"Resistir ante la insistencia de alguien para que hagas algo"),
(17,4,"¿Haces elogios a un amigo?"),
(18,4,"Solicitar un aumento de sueldo o plantear una reclamación ante un superio"),
(19,4,"Preguntar a alguien si le has ofendido"),
(20,4,"Decir a alguien que no te cae bien");

//insert para las preguntas de Test de Drogas

insert INTO questions values
(21,5,"Cada cuanto piensas que tienes un problema con drogas"),
(22,5,"Cada cuanto fuiste atendido médicamente por razones que están ligadas a tu consumo de drogas"),
(23,5,"Cada cuanto piensas que tienes un problema con drogas"),
(24,5,"Se han quejado alguna vez tus familiares por tu consumo de drogas"),
(25,5,"Se han visto afectadas tus relaciones de pareja por consumir drogas");


//insert para las preguntas de Test de Ansidad

insert INTO questions values
(26,6,"Decir que no cuando te piden dinero prestado"),
(27,6,"Pedir una cita a alguien que ya te rechazó en una ocasión"),
(28,6,"Pedir críticas constructivas"),
(29,6,"Admitir tus dudas sobre algún aspecto que se esté discutiendo y pedir que te lo aclaren"),
(30,6,"Devolver cosas defectuosas o en mal estado");

//insert para las preguntas de Test de mi estado de  salud

insert INTO questions values
(31,7,"¿Tienes problemas visuales?"),
(32,7,"Sufres contracturas musculares"),
(33,7,"Notas sabor amargo en la boca"),
(34,7,"Uñas quebradizas"),
(35,7,"Tienes dolor sobre el flanco derecho");

Estas levemente con problemas de salud, puedes ayudarte haciendo ejecicio
Estas con bastantes problemas de salud, debes ir de inmediato al doctor
Estas con ciertos problemas de salud, podrias sacar algun tiempo para acudir al medido de manera preventiva






//insert para las respuestas del test de salud


INSERT INTO answers VALUES  
(16,6,1,'Nunca',5),
(17,6,2,'Solo un poco',10),
(18,6,3,'Algunos veces',15),
(19,6,4,'Muy frecuentemente',20);
INSERT INTO answers VALUES  
(20,7,1,'Nunca',5),
(21,7,2,'Solo un poco',10),
(22,7,3,'Algunos veces',15),
(23,7,4,'Muy frecuentemente',20);
INSERT INTO answers VALUES 
(24,8,1,'Nunca',5),
(25,8,2,'Solo un poco',10),
(26,8,3,'Algunos veces',15),
(27,8,4,'Muy frecuentemente',20);
INSERT INTO answers VALUES 
(28,9,1,'Nunca',5),
(29,9,2,'Solo un poco',10),
(30,9,3,'Algunos veces',15),
(31,9,4,'Muy frecuentemente',20);
INSERT INTO answers VALUES 
(32,10,1,'Nunca',5),
(33,10,2,'Solo un poco',10),
(34,10,3,'Algunos veces',15),
(35,10,4,'Muy frecuentemente',20);



//insert para las respuestas del test de amistad 

INSERT INTO answers VALUES 
(36,11,1,'No le hablo nunca más',10),
(37,11,2,'Le pido explicaciones, y quizás le dé otra chance',15),
(38,11,3,'Seguro lo perdono: la amistad es sagrada',20),

(39,12,1,'Depende; debería pensar en las consecuencias',10),
(40,12,2,'Sí, pero buscaría la manera de no salir perjudicado',15),
(41,12,3,'Sí, siempre',20),

(42,13,1,'Soy igual con todos, quizás porque con ninguno tengo una relación profunda',10),
(43,13,2,'Tengo un solo amigo del corazón, y muchos con los que tengo una excelente relación.',15),
(44,13,3,'No hago diferencia entre quienes considero mis amigos',20),

(45,14,1,'Sin darle explicación, empiezo a dejar de ver a mí amigo',10),
(46,14,2,'Hablo con el para aclarar la situación',15),
(47,14,3,'Me retiro de la pugna, sin revelar mis sentimientos.',20),

(48,15,1,'A veces',10),
(49,15,2,'La Mayoria',15),
(50,15,3,'Si,siempre',20);


//insert para la respuestas de test de Habilidades
insert into answers VALUES
(58,38,1,'Siempre lo hago',20),
(59,38,2,'Lo hago ocasionalmente',15),
(60,38,3,'Nunca o rara vez lo hago',10),

(61,39,1,'Nunca o rara vez lo hago',20),
(62,39,2,'Siempre lo hago',15),
(63,39,3,'Lo hago ocasionalmente',10),

(64,40,1,'Siempre lo hago',20),
(65,40,2,'Lo hago ocasionalmente',15),
(66,40,3,'Nunca o rara vez lo hago',10),

(67,41,1,'Nunca o rara vez lo hago',20),
(68,41,2,'Siempre lo hago',15),
(69,41,3,'Lo hago ocasionalmente',10),

(70,42,1,'Siempre lo hago',20),
(71,42,2,'Lo hago ocasionalmente',15),
(72,42,3,'Nunca o rara vez lo hago',10);

//insert para la respuestas de test de Drogas
insert into answers VALUES
(73,43,1,'Nunca',10),
(74,43,2,'Rara vez',15),
(75,43,3,'Ocasionalmente',20),

(76,44,1,'Nunca',10),
(77,44,2,'Rara vez',15),
(78,44,3,'Ocasionalmente',20),

(79,45,1,'Nunca',10),
(80,45,2,'Rara vez',15),
(81,45,3,'Ocasionalmente',20),

(82,46,1,'Nunca',10),
(83,46,2,'Rara vez',15),
(84,46,3,'Ocasionalmente',20),

(85,47,1,'Nunca',10),
(86,47,2,'Rara vez',15),
(87,47,3,'Ocasionalmente',20);

//insert para la respuestas de test de Ansiedad
insert into answers VALUES
(88,48,1,'Nunca',10),
(89,48,2,'Rara vez',15),
(90,48,3,'Ocasionalmente',20),

(91,49,1,'Nunca',10),
(92,49,2,'Rara vez',15),
(93,49,3,'Ocasionalmente',20),

(94,50,1,'Nunca',10),
(95,50,2,'Rara vez',15),
(96,50,3,'Ocasionalmente',20),

(97,51,1,'Nunca',10),
(98,51,2,'Rara vez',15),
(99,51,3,'Ocasionalmente',20),

(100,52,1,'Nunca',10),
(101,52,2,'Rara vez',15),
(102,52,3,'Ocasionalmente',20);

//insert para la respuestas de test de Salud  de mi estado de  salud

insert into answers VALUES
(103,53,1,'Nunca',10),
(104,53,2,'A veces',15),
(105,53,3,'Frecuentemente',20),

(106,54,1,'Nunca',10),
(107,54,2,'A veces',15),
(108,54,3,'Frecuentemente',20),

(109,55,1,'Nunca',10),
(110,55,2,'A veces',15),
(111,55,3,'Frecuentemente',20),

(112,56,1,'Nunca',10),
(113,56,2,'A veces',15),
(114,56,3,'Frecuentemente',20),

(115,57,1,'Nunca',10),
(116,57,2,'A veces',15),
(117,57,3,'Frecuentemente',20);




//insert la retroalimentacion test de trabajo
INSERT INTO results VALUES 
(1,1,25,50,'No tienes madera de líder
No demuestras tener casi ningún rasgo que denote que eres un líder. Prefieres que otras personas decidan por 
ti y te gusta pasar desapercibido en la mayoría de las situaciones. Pero ya sabes el refrán …”querer es poder”así que con empeño y
 determinación podrías reunir todas las cualidades necesarias para ser considerado un buen líder.'),
(2,1,51,70,'Puedes llegar a ser un líder
Aunque no puedas ser reconocido como un líder nato, demuestras poseer la capacidad de implicarte y 
actuar como tal en ciertas ocasiones así que ¡no está todo perdido! Si de verdad quieres convertirte en 
un buen líder, detecta las carencias que tienes y trabájalas para conseguir tus objetivos en la vida'),
(3,1,71,100,'Eres un auténtico líder y no puedes disimularlo. La gente de tu entorno te respeta y reconoce como una persona
de referencia en la que confiar. Tu comportamiento tanto dentro como fuera del trabajo no deja lugar a duda, 
sabes dirigir, coordinar, convencer ayudar y motivar a todas las personas que tienes alrededor.');


//insert la retroalimentacion de salud

INSERT INTO results VALUES 
(4,2,25,50,'Trastorno Bipolar leve
  En muchos casos, estos síntomas no afectan 
el funcionamiento normal de una persona. Consulta con un profesional de salud mental
si estás experimentando sentimientos depresivos y/o dificultades en el funcionamiento cotidiano.'),
(5,2,51,70,'Trastorno Bipolar con síntomas moderados. Las personas que han respondido de 
 manera similar normalmente califican para un diagnóstico de trastorno bipolar I o II, y han recibido
 tratamiento profesional para este trastorno. '),
 (6,2,71,100,'Trastorno bipolar con síntomas graves. . Las personas que han 
 respondido de manera similar normalmente califican para un diagnóstico de trastorno bipolar I o II,
 y han recibido tratamiento profesional para este trastorno. 
 Sería beneficioso que busques un diagnóstico más profundo de un profesional de la salud mental.');

 //insert de retroalimentacion de Amistad

INSERT INTO results VALUES 
(7,3,25,50,'La amistad no se encuentra en la cima de tus prioridades en la vida. Eres bastante escéptico y te ilusionas poco.
 Aunque tengas algunos amigos, mantienes una relación más bien fría y superficial. Si bien hay otras cosas importantes,
 la amistad es un bien muy valioso que quizás estés perdiendo. Intenta ser más abierto.'),
(8,3,51,70,' Te gusta cultivar la amistad. Te gusta dar y recibir afecto, por lo que suelen considerarte confiable y buen amigo. 
 Pero tu eres selectivo: solo quienes cumplen ciertos requisitos son parte de tu circulo intimo. Tienes muchos conocidos y
 pocos amigos. Además prefieres escuchar antes que hablar, y no confías a cualquiera tus secretos. '),
 (9,3,71,100,'La amistad es para ti un valor fundamental, tan importante y gratificante como el amor. 
 Te sientes vivo, pleno y aceptado solamente estando entre tu grupo de amigos. Puedes dejar todo por ellos. Una advertencia: estar enamorado 
 de la amistad puede generarte cierta dependencia. Como en todo en la vida, busca el equilibrio.');
 
 //insert de retroalimentacion de Drogas
 INSERT INTO results VALUES 
(13,42,25,50,'Tus respuestas no sugieren ningún grado de dependencia. Probablemente las has utilizado alguna vez y te sientes mal por ello.'),


(14,42,51,70,'Basados en tus respuestas parece que existe un uso y abuso de sustancias. Tu dependencia está en fase inicial a media, y experimentas problemas ocasionales o frecuentes relacionados con esta adicción. Sería imprescindible que obtengas más información sobre la dependencia química y comiences a trabajar en tu recuperación.'),


(15,42,71,100, 'Tu patrón de respuestas señala una etapa de dependencia a drogas avanzada, que causa problemas serios en tu vida. Te beneficiarias de buscar ayuda en los recursos apropiados. Pide una entrevista con tu médico y ponte en serio a trabajar sobre ese tema.');
 
 
 //insert de retroalimentacion Test de habilidades Sociales
 INSERT INTO results VALUES 
 
(7,3,25,50,'. La persona asertiva expresa su opinión de manera apropiada y defiende su punto de vista, siempre respetando la opinión de los demás'),
(8,3,51,70,'La validación emocional es el entendimiento y expresión de la aceptación de la experiencia emocional de la otra persona, y
 mejora las relaciones interpersonales puesto que el otro 
interlocutor se siente comprendido y reconocido'),
 (9,3,71,100,'Vivir la vida de manera negativa va a influir en cómo vemos el mundo y, por ende, cómo nos relacionamos con los demás');
 
 //insert de retroalimentacion Test de Ansiedad
 
INSERT INTO results VALUES 

(16,43,25,50,'Nada te pone nervioso ni altera tu equilibrio emocional. 
Tener que enfrentarte a situaciones que para otros serían angustiosas te parecen un puro trámite. '),



(17,43,51,70,'Te encuentras en un término medio entre las personas excesivamente controladas y seguras y las que se preocupan por todo, incluso sin motivo. '),



(18,43,71,100, 'Vives al borde de un ataque de nervios. Estás envuelto por una atmósfera de preocupación por lo que pueda pasar, que en la mayoría de los casos, 
si te pararas a pensarlo, carece de fundamento. '  );


// insert de retroalimentacion de estado de salud

INSERT INTO results VALUES 

(19,44,25,50,"Estas levemente con problemas de salud, puedes ayudarte haciendo ejecicio"),
(20,44,51,70,"Estas con bastantes problemas de salud, debes ir de inmediato al doctor"),
(21,44,71,100,"Estas con ciertos problemas de salud, podrias sacar algun tiempo para acudir al medido de manera preventiva");




 
//insert lista de cuestionarios

INSERT INTO questionnaires VALUES 
(1,"Test de Trabajo", 'Test para saber si soy líder!!Si quieres descubrir si eres un auténtico líder , contesta a las preguntas del siguiente test de personalidad sobre liderazgo.'),
(2,"Test de bipolaridad", 'Realizando el siguiente test de bipolaridad podrás tener una orientación de si tus comportamientos o los de una persona cercana son normales o son debido a un trastorno de bipolaridad.'),
(3,"Test de Amistad", 'Hay quienes consideran a los amigos un verdadero tesoro y mantienen sus conversaciones más profundas con ellos. Otros sólo los tienen cerca cuando presenta algún problema. Responde con sinceridad a estas preguntas y descubre que valor le das a la amistad.')
(4,"Test de Habilidades Sociales",'Este test te ayudará a percibir aquellas habilidades sociales que necesitas mejorar para conseguir un trabajo.'),
(5,"Test de Drogas",'El test de drogas no es un reemplazo a la evaluación profesional, sino una herramienta más para el autodiagnóstico')
(6,"Test de Ansiedad", 'test para que puedas realizar una rápida evaluación de tu nivel de ansiedad, basándose en tu respuesta emocional según diversas situaciones de la vida real.'),
(7,"Test de mi estado de  salud",'El objetivo de este test para evaluar el estado de tu salud según la medicina tradicional china, es que puedas reconocer cuál o cuáles son los órganos que predominan en su esquema corporal, de esta manera podrás conocerte a ti mismo y resolver algunos problemas de salud');









